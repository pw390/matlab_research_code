function [ weight] = gamma_flex_mid_price_weight(parameter,bid_side,ask_side,tick_size)
    [m,n] = size(parameter);
    weight = zeros(n,1);
    for i  = 1:n
        % bid side
        price = bid_side.price(bid_side.price <= parameter(5,i));
        quantity = bid_side.Q(bid_side.price <= parameter(5,i));
        price_used = parameter(5,i) - price;
        front_price = price_used -0.5*tick_size;
        front_price(front_price<0) = 0;
        back_price = price_usde  + 0.5*tick_size;
        cdf_front = gamcdf(front_price,parameter(1,i),paramter(2,i));
        cdf_back = gamcdf(back_price,parameter(1,i),paramter(2,i));
        cdf = cdf_back-cdf_front;
        cdf(cdf==0)=tick_size*gampdf(price_used(cdf==0),...
                            parameter(1,i),paramter(2,i));
        
        weight(i,1) = weight(i,1)+ sum(quantity.*log(cdf));
        
        % ask side
        price = ask_side.price(ask_side.price <= parameter(5,i));
        quantity = ask_side.Q(ask_side.price <= parameter(5,i));
        price_used = -parameter(5,i) + price;
        front_price = price_used -0.5*tick_size;
        front_price(front_price<0) = 0;
        back_price = price_usde  + 0.5*tick_size;
        cdf_front = gamcdf(front_price,parameter(3,i),paramter(4,i));
        cdf_back = gamcdf(back_price,parameter(3,i),paramter(4,i));
        cdf = cdf_back-cdf_front;
        cdf(cdf==0)=tick_size*gampdf(price_used(cdf==0),...
                            parameter(3,i),paramter(4,i));
        
        weight(i,1) = weight(i,1)+ sum(quantity.*log(cdf));
        
    end
end

