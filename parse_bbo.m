%% load bbo data
% first filen name for bbo
CurrentDir = pwd;
filename_1 = 'BBO_2.csv';
%Load Snapshot data that includes all price levels
Infile = fullfile(CurrentDir,'Hourly',filename_1);
fid = fopen(Infile);
BBO_data = textscan(fid,'%f %f %f %f %f','Delimiter',','); 
fclose(fid);
%%
bbo_time = BBO_data{1};
best_bid = BBO_data{2};
best_ask= BBO_data{4};
bbo_mid_price = 0.5*(best_bid + best_ask);
%%
filename = 'SNP_2.csv';
[bid_side, ask_side, snp_mid_price,snp_time] = Data_parse_primitive(filename,5e4);


%%

plot(snp_time,snp_mid_price);
hold on
plot(bbo_time,bbo_mid_price);
xlabel('Time')
ylabel('Price')
legend('SNP data', 'BBO data')