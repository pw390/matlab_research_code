%% add path
addpath('Hourly');
addpath('Summer');
addpath('matlab');

%%
filename = 'SNP_3.csv';
[bid_side, ask_side, mid_price_l,time] = Data_parse_primitive(filename,5e4);
%% pre-estimate variance of alpha and beta
N = length(bid_side);
bid_parameter = zeros(2,N);
ask_parameter = zeros(2,N);
for i = 1:N
    phi = gamfit(abs(bid_side(i).price - mid_price_l(i)),[],[],bid_side(i).Q);
    bid_parameter(:,i) = phi';
    phi= gamfit(abs(ask_side(i).price - mid_price_l(i)),[],[],ask_side(i).Q); 
    ask_parameter(:,i) = phi'; 
end
%% estimation
mean_bid = mean(bid_parameter');
v_bid = std(bid_parameter');
mean_ask = mean(ask_parameter');
v_ask = std(ask_parameter');
%% both side estimation
start_time = 700;
end_time = 800;
T = end_time - start_time +1;

sample_number = 1000;
sigma_mid_price = 1e-5;
tick_size = 1e-5;

parameter = [mean_bid*ones(2,sample_number);....
            mean_ask*ones(2,sample_number);...
            mid_price_l(start_time)*ones(1,sample_number)];
%%
for t = start_time : end_time
    disp(t)
    parameter = gamma_flex_mid_price_sample( parameter,...
                                    v_bid,v_ask, v_mid_price);
    weight = gamma_flex_mid_price_weight(parameter,bid_side(t),ask_side(t),tick_size);
    weight = weight_normalisation(weight);
    
    
end















