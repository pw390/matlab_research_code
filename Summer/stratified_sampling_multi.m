function [new_a,new_b,new_mu,new_weight ] = stratified_sampling_multi( a,b,mu,weight)

N = length(a);
c = cumsum(weight);
new_a = zeros(N,1);
new_b = zeros(N,1);
new_mu = zeros(N,1);
for r =1 : N
    u  = (rand()+(r-1))/N;
    i = find((c >= u),1);
    new_a(r) = a(i,1);
    new_b(r) = b(i,1);
    new_mu(r) = mu(i,1);
end
new_weight = 1/N *ones(N,1);

end

