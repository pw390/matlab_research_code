%% shape approximation of Gamma and alpha-stable 
% gamma distribution can be fitted by alpha -stable
figure
k= 3;
theta = 0.5;
N =1000000;
bin = 200;
x= gamrnd(k,theta,N,1);
[counts,centers]=hist(x,bin);
w = centers(2)-centers(1);
bar(counts)
y = centers(1): w : centers(bin);
hold on
par = stable_fit_init(x);
par = stable_fit_mle2d(x,par);
pdf = stable_pdf(y,par);
plot(pdf*N*w,'b');


hold on
gpdf = gampdf(y,k,theta);
plot(gpdf*N*w,'r')

hold on
[phat,pci] = gamfit(x);
gpdf2 = gampdf(y,phat(1),phat(2));
plot(gpdf2*N*w,'o')


