function [log_ll] = log_multinomial(alpha,bid_side,support_num,tick_size)
    hist_count= zeros(support_num,1);
    for j = 1 : length(bid_side.abs_diff)
       ind = floor(bid_side.abs_diff(j)/tick_size)+1;
       if ind <support_num
       hist_count(ind,1) = hist_count(ind,1) + round(bid_side.Q(j));
       end
    end
    log_ll = 0;
    log_ll =log_ll + log(gamma(sum(hist_count)+1))-sum(log(gamma(hist_count+1)));
    disp(log_ll)
    log_ll = log_ll + log(gamma(sum(alpha)))-sum(log(gamma(alpha)));
    disp(log_ll)
    log_ll = log_ll + log(gamma(sum(alpha+hist_count)))-sum(log(gamma(alpha+hist_count)));
    disp(log_ll)
end

