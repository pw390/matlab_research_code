%% reconstruction LOB
CurrentDir = pwd;
filename_1 = 'SNP_3.csv';
%Load Snapshot data that includes all price levels
Infile = fullfile(CurrentDir,'Hourly',filename_1);
fid = fopen(Infile);
Snapshot_Data = textscan(fid,'%f %f %f %f','Delimiter',','); 
fclose(fid);


Time = Snapshot_Data{1};
Type = Snapshot_Data{2};
Price_Y = Snapshot_Data{3}; 
Quantity_Z = Snapshot_Data{4};
%%
figure
start_time = 700; % start_time
end_time = 800; % end_time
unique_time = unique(Time);
N = length(unique_time);

%time_stamp = unique_time(T);
% pos = find(Time == time_stamp);
% last_pos = pos(end);
% Quantity_selected = Quantity_Z(1:last_pos);
% Time_selected = Time(1:last_pos);
% Price_selected = Price_Y(1:last_pos);
% [Zs,ind] = sort(Quantity_selected);
% Xs = Time_selected(ind);
% Ys = Price_selected(ind);

for i = start_time : end_time
    time_stamp = unique_time(i);
    t = find(Time == time_stamp);
    bid_ind = t(Type(t)==1);
    bid_price = Price_Y(bid_ind);
    bid_quantity = Quantity_Z(bid_ind);
    max_bid = max(bid_quantity );
    ask_ind = t(Type(t)==2);
    ask_price = Price_Y(ask_ind);
    ask_quantity = Quantity_Z(ask_ind);
    max_ask = max(ask_quantity );
    temp_max = max(max_ask, max_bid);
    if isempty(ask_price) 
        best_ask  = 0;
    else
        best_ask = min(ask_price);
    end
    
    
    if isempty(bid_price)
        best_bid = 0;
    else
        best_bid = max(bid_price);
    end
    
    if best_bid == 0 &&  best_ask == 0
       mid_price= mid_price_l(i-1);
    else
        if best_bid == 0
            mid_price = mid_price_l(i-1);
%             mid_price= best_ask;
        else
            if best_ask == 0
                mid_price = mid_price_l(i-1);
%                 mid_price = best_bid;
            else
                mid_price = (best_ask + best_bid)/2;
            end
        end
    end
    
    for j = 1: length(bid_price)
        if(abs(bid_price(j)-mid_price)/1e-5<=80)
            rectangle('Position',[i,bid_price(j)-0.00001/2,1,0.00001],'Facecolor',...
            hsv2rgb([2/3-2/3*bid_quantity(j)/temp_max,1.0,1.0]),'edgecolor','none') 
        end
    end
    
    for j = 1: length(ask_price)
        
        if(abs(ask_price(j)-mid_price)/1e-5<=80)
            rectangle('Position',[1,ask_price(j)-0.00001/2,1,0.00001],'Facecolor',...
            hsv2rgb([2/3-2/3*ask_quantity(j)/temp_max,1.0,1.0]),'edgecolor','none')
        end
    end
       
end
xlim([start_time, end_time])
hold on

% %% multinomial distribution
% 
% % dirichlet distribution
% filename = 'SNP_2.csv';
% [bid_side, ask_side, mid_price_l ] = Data_parse(filename,1e7,1);
% 
% %%
% T = 100; 
% limit_depth = 50;
% tick_size = 0.00001;
% support_num = limit_depth;
% hyper_ask= ones(support_num,1);
% hyper_param_map_ask= zeros(support_num, T);
% hyper_bid = ones(support_num,1);
% hyper_param_map_bid = zeros(support_num,T);
% hist_count = zeros(support_num,1);
% 
% for i = 1:T
%     for j = 1 : length(bid_side(i).abs_diff)
%        ind = floor(bid_side(i).abs_diff(j)/tick_size)+1;
%        if ind <support_num
%        hist_count(ind,1) = hist_count(ind,1) + round(bid_side(i).Q(j));
%        end
%     end
%     hyper_bid  = hyper_bid + hist_count;
%     hyper_param_map_bid(:,i) = hyper_bid;
%     hist_count = zeros(support_num,1); 
%     for j = 1 : length(ask_side(i).abs_diff)
%        ind = floor(ask_side(i).abs_diff(j)/tick_size)+1;
%        if ind <support_num
%        hist_count(ind) = hist_count(ind) + round(ask_side(i).Q(j));
%        end
%     end
%     hyper_ask = hyper_ask + hist_count;
%     hyper_param_map_ask(:,i) = hyper_ask;
% end
% %% plot 
% figure(1)
% tick_size = 0.00001;
% for j=1:T
%     hyper_param_map_ask(:,j) = hyper_param_map_ask(:,j)/sum(hyper_param_map_ask(:,j));
%     hyper_param_map_bid(:,j) = hyper_param_map_bid(:,j)/sum(hyper_param_map_bid(:,j));
%     temp_color =hyper_param_map_ask(:,j) ;
%     temp_max = max(temp_color);
%     for d = 1 : support_num
%         %%Construct heat map with position defined by price and time, colour
%         %%defined by quantity
%     rectangle('Position',[j-1,mid_price_l(j)+(d-1)*tick_size,1,tick_size ],'Facecolor'...
%        ,hsv2rgb([2/3-2/3*temp_color(d)/temp_max,1.0,1.0]),'edgecolor','none'); 
%     end
%     temp_color =hyper_param_map_bid(:,j);
%     temp_max = max(temp_color);
%     for d = 1 : support_num
%         %%Construct heat map with position defined by price and time, colour
%         %%defined by quantity
%     rectangle('Position',[j-1,mid_price_l(j)-d*tick_size,1,tick_size ],'Facecolor'...
%        ,hsv2rgb([2/3-2/3*temp_color(d)/temp_max,1.0,1.0]),'edgecolor','none'); 
%     end
%     
% end
% hold on 
% %%git 
% plot(mid_price_l(1:T),'o')

%%
filename = 'SNP_3.csv';
[bid_side, ask_side, mid_price_l, Time] = Data_parse(filename,5e4,1);
%%
start_time = 700;
end_time = 800;
tick_size = 1e-5;
T_duration = end_time - start_time+1;
Truncate = 80;
if Truncate>0
    for i = 1:length(ask_side)
        t_a= find(ask_side(i).abs_diff <=Truncate*tick_size);
        t_b= find(bid_side(i).abs_diff <=Truncate*tick_size);        
        ask_side(i).abs_diff = ask_side(i).abs_diff(t_a);
        ask_side(i).Q = ask_side(i).Q(t_a);
        bid_side(i).abs_diff = bid_side(i).abs_diff(t_b);
        bid_side(i).Q = bid_side(i).Q(t_b);
    end
end
figure
i =1;
for t = start_time : end_time
    disp(t)
    
    temp_max= max(max(ask_side(t).Q),max(bid_side(t).Q));
    for d = 1 : length(bid_side(t).abs_diff)
        %%Construct heat map with position defined by price and time, colour
        %%defined by quantity
    rectangle('Position',[t,mid_price_l(t)-bid_side(t).abs_diff(d),1,tick_size],'Facecolor'...
       ,hsv2rgb([2/3-2/3*bid_side(t).Q(d)/temp_max,1.0,1.0]),'edgecolor','none'); 
    end

    for d = 1 : length(ask_side(t).abs_diff)
        %%Construct heat map with position defined by price and time, colour
        %%defined by quantity
    rectangle('Position',[t,mid_price_l(t)+ask_side(t).abs_diff(d),1,tick_size],'Facecolor'...
       ,hsv2rgb([2/3-2/3*ask_side(t).Q(d)/temp_max,1.0,1.0]),'edgecolor','none'); 
    end
    i= i+1;
end
xlim([start_time,end_time]);
hold on 
plot(start_time:end_time,mid_price_l(start_time:end_time),'-','color','k','LineWidth',2);
ylabel('Price [USD/EUR]','FontSize',18);xlabel('Time','FontSize',18);
%title('Heat map of orderbook rebuilt based on Gamma distribution with Quantity (reference price: mid-price)');
colorbar();caxis([0 1]);
hold on

