function [ weight ] = log_gamma( y,x)
% This function log particle weight

N = length(y.abs_diff);
weight = 0 ;
for i = 1 : N 
    weight = weight + y.Q(i)* log(gampdf(y.abs_diff(i)+x.s, x.alpha, x.beta ));
end

end

