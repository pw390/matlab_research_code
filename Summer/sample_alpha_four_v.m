function [a,b,c,mu ] = sample_alpha_four_v( old_a,old_b,old_c,old_mu,sigma_a, sigma_b, sigma_c,sigma_mu )
    a = 0*old_a;
    b = 0*old_b;
    mu = 0*old_mu;
    c = 0* old_c;
    for i = 1:length(a)
      
        temp = gamrnd(old_a(i)^2/sigma_a^2,sigma_a^2/old_a(i));
        while temp<=0.5 || temp >1.5
            temp = gamrnd(old_a(i)^2/sigma_a^2,sigma_a^2/old_a(i));
        end
        a(i) = temp;
        temp = normrnd(old_b(i), sigma_b);
        while temp<0.6 || temp >1
              temp = normrnd(old_b(i), sigma_b);
        end
        b(i) = temp;
        mu(i) = normrnd(old_mu(i),sigma_mu);
        c(i)= gamrnd(old_c(i)^2/sigma_c^2,sigma_c^2/old_c(i));
    end

end

