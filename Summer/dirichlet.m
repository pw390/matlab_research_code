function [prior_alpha] = dirichlet(a,b,c,mu,support_num,tick_size)
N = length(a);
prior_alpha = zeros(support_num,N);
parfor i = 1:N
    pd1 = makedist('Stable','alpha',a(i),'beta',b(i),'gam',c,'delta',mu(i));
    x = (1:support_num)*tick_size;
    %cdf1 = cdf(pd1,x); 
    %x = (0:support_num-1)*tick_size;
    %cdf2 = cdf(pd1,x); 
    prior_alpha(:,i) = pdf(pd1,x)';
    prior_alpha(:,i)= prior_alpha(:,i)/sum(prior_alpha(:,i));
end

end
