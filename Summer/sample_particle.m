function [a,b,mu] = sample_particle(N,old_a,old_b,old_mu,sigma_a, sigma_b, sigma_mu)
    a = zeros(N,1);
    b = zeros(N,1);
    mu = zeros(N,1);
    parfor i = 1:N
        temp = gamrnd(old_a(i)^2/sigma_a^2,sigma_a^2/old_a(i));
        while temp<=0.5 || temp >1.5
            temp = gamrnd(old_a(i)^2/sigma_a^2,sigma_a^2/old_a(i));
        end
        a(i) = temp;
        temp = normrnd(old_b(i), sigma_b);
        while temp<0.6 || temp >1
              temp = normrnd(old_b(i), sigma_b);
        end
        b(i) = temp;
        mu(i) = normrnd(old_mu(i),sigma_mu);
    end
end

