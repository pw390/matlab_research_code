%% ReadIn Data
filename = 'SNP_2.csv';
[bid_side, ask_side, mid_price_l] = Data_parse_primitive(filename,1e5);

%% parameter
sigma_a = 0.3;
sigma_b = 0.2;
sigma_mu =1e-5;
sigma_m = 0;
sigma_theta = 1e-5;
alpha = -0.2;
c = 5e-5;
tick_size = 1e-5;
below_base = 40;

N = 800; % particle number
start_time = 151; % start_time
end_time =350; % end_time
T_duration = end_time - start_time+1;
support_num = 80; % support number of multinomial distribution
bid_parameter = [ones(N,1),0.8*ones(N,1),3*tick_size*ones(N,1)];
ask_parameter = [ones(N,1),0.8*ones(N,1),3*tick_size*ones(N,1)];
price_state = [mid_price_l(start_time)*ones(N,1),0*ones(N,1)];

bid_param_map = zeros(3,T_duration);
ask_param_map = zeros(3,T_duration);
price_state_map = zeros(2,T_duration);
weight_map = zeros(N,T_duration);


%% skeleton
j = 1 ;
for t = start_time : end_time
    disp(t)
    [bid_parameter,ask_parameter,price_state] = sample_both_side(N,...
        bid_parameter,ask_parameter,price_state,...
            sigma_a, sigma_b, sigma_mu,sigma_m,sigma_theta,alpha);
     weight =  log_both_side( bid_parameter,ask_parameter,c,...
        price_state,bid_side(t), ask_side(t),support_num, below_base,tick_size,N);
    weight_map(:,j) = weight;
    weight  = weight_normalisation(weight);
    [bid_parameter, ask_parameter, price_state] = stratified_sampling_both_side(...
                bid_parameter,ask_parameter,price_state,weight);

    
    bid_param_map(:,j)= mean(bid_parameter).';
    ask_param_map(:,j) = mean(ask_parameter).';
    price_state_map(:,j)= mean(price_state).';
    
    j = j+1;
end

%% log-likelihood

marginal_ll=0;
for i = 1: T_duration
    max_weight = max(weight_map(:,i));
    marginal_ll= marginal_ll+max(weight_map(:,i));
    n_weight = exp(weight_map(:,i) - max_weight);
    marginal_ll = marginal_ll+log(sum(n_weight));
end
disp(marginal_ll)

%% plot method 2
figure
i =1;
for t = start_time : end_time-5
    disp(t)
    balpha = dirichlet(bid_param_map(1,i),bid_param_map(2,i),c,bid_param_map(3,i),...
        support_num-below_base,tick_size);
    balpha = balpha/sum(balpha);
    temp_max= max(balpha);
    
    salpha = dirichlet(ask_param_map(1,i),ask_param_map(2,i),c,ask_param_map(3,i),...
        support_num-below_base,tick_size);
    salpha = salpha/sum(salpha);
    temp_max= max(max(salpha),temp_max);
    for d = 1 : support_num-below_base
        %%Construct heat map with position defined by price and time, colour
        %%defined by quantity
    rectangle('Position',[i,price_state_map(1,i)-d*tick_size,1,tick_size],'Facecolor'...
       ,hsv2rgb([2/3-2/3*balpha(d)/temp_max,1.0,1.0]),'edgecolor','none'); 
    end

    for d = 1 : support_num-below_base
        %%Construct heat map with position defined by price and time, colour
        %%defined by quantity
    rectangle('Position',[i,price_state_map(1,i)+(d-1)*tick_size,1,tick_size],'Facecolor'...
       ,hsv2rgb([2/3-2/3*salpha(d)/temp_max,1.0,1.0]),'edgecolor','none'); 
    end
    i= i+1;
end
hold on
plot(price_state_map(1,1:end-5),'yo')
plot(mid_price_l(start_time:end_time-5),'k')