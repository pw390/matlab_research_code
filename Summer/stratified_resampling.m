function [ new_x, new_weight] = stratified_resampling(x,weight)
% input x is a struct with three field 'alpha', 'beta', 's'
% weight is column vector is normalised weight

N = length(x);
c = cumsum(weight);
new_alpha = zeros(N,1);
new_beta = zeros(N,1);
new_s = zeros(N,1);
for r =1 : N
    disp(r)
    u  = (rand()+(r-1))/N;
    i = find((c >= u),1);
    new_alpha(r) = x(i).alpha;
    new_beta(r) = x(i).beta;
    disp(i)
    new_s(r) = x(i).s;
end
new_x = struct('alpha',num2cell(new_alpha),...
        'beta',num2cell(new_beta),'s',num2cell(new_s));
new_weight = 1/N *ones(N,1);
end

