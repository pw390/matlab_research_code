%To repeat the same process of Lin's to get 
filename = 'SNP_3.csv';
[bid_side, ask_side, mid_price_l, Time] = Data_parse(filename,5e4,1);
%% load learning parameter
load ('gam_ask_param.mat');
load('gam_bid_param.mat');


%% training set formulation and test set formation
start_time = 700;
end_time = 800;
T = end_time -start_time +1;
% price change
dpt = mid_price_l(start_time+1:end_time)- mid_price_l(start_time:end_time-1);
% expeted f_t
f_t = dpt./mid_price_l(start_time:end_time-1);
Q_a = zeros(Turaton,1);
Q_b = zeros(Turaton,1);
for i = start_time : end_time
    Q_a(i-start_time+1)= sum(ask_side(i).Q);
    Q_b(i-start_time+1)= sum(bid_side(i).Q);
end
%% create feature set

mode = 3;
switch mode
    case 1
      feature=zeros(T,8);
      feature(:,1:3)= bid_param_map';
      feature(:,4:6)= ask_param_map';
      feature(:,7) = Q_a;
      feature(:,8) = Q_b;
    case 2
      feature=zeros(T,14);
      feature(:,1:3)= bid_param_map';
      feature(:,4:6)= ask_param_map';
      feature(:,7) = Q_a;
      feature(:,8) = Q_b;
      feature(:,9:11)= bid_param_map.^2';
      feature(:,12:14)= ask_param_map.^2';
    case 3
      feature=zeros(T,10);
      feature(:,1:3)= bid_param_map';
      feature(:,4:6)= ask_param_map';
      feature(:,7) = Q_a;
      feature(:,8) = Q_b;
      feature(:,9)= Q_a.^2';
      feature(:,10)= Q_b.^2';
end
    

% permutation
n = length(f_t);
p = randperm(n);
n_training = round(0.6*n);
n_test = n - n_training;
% training_set = f_t(p(1:n_training));
% test_set = f_t(p(n_training+1:n));
% training_feature = feature(p(1:n_training),:);
% test_feature = feature(p(n_training+1:n),:);

training_feature = feature(1:n_training,:);
[training_feature ,mu, sigma] = featureNormalize(training_feature);
training_feature = [ones(n_training,1),training_feature];

test_feature = feature(n_training+1:n,:);
tf_mu = test_feature - repmat(mu,length(test_feature),1);
tf_std = repmat(sigma,length(test_feature),1);
test_feature = tf_mu ./ tf_std;
test_feature = [ones(n_test,1),test_feature];

training_set = f_t(1:n_training);
% test_set = f_t(p(n_training+1:n));

%% training and plot training reuslt

alpha = 0.01;
num_iters= 10000;

theta = zeros(size(training_feature,2), 1);
[theta, J_history] = gradientDescentMulti(training_feature, training_set', theta, alpha, num_iters);

%% test and plot test result
figure
result = [training_feature;test_feature]*theta;
plot(result)
hold on 
plot(f_t)
legend('training','actual')


%% model the price change
price_t = zeros(n+1,1);
dprice_t = zeros(n+1,1);
price_t(1)= mid_price_l(start_time);
the = 0;
for i = 2:n+1
    price_t(i) = price_t(i-1)+dprice_t(i-1);
    dprice_t(i)  = result(i-1)*price_t(i-1)+the*randn();
end
figure
plot(mid_price_l(start_time:end_time))
hold on
plot(price_t)

figure
plot(dprice_t)
hold on 
plot(dpt)