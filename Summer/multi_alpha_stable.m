%% ReadIn Data
filename = 'SNP_2.csv';
[bid_side, ask_side, mid_price_l ] = Data_parse(filename,1e7,1);
%%
sigma_a = 0.25;
sigma_b = 0.01;
sigma_mu = 4000e-10;

N = 100; % particle number
start_time = 1; % start_time
end_time = 50; % end_time
T_duration = end_time - start_time+1;
support_num = 50; % support number of multinomial distribution
param_map = zeros(3,T_duration);
c = sqrt(1.3588e-8);
[a,b, mu] = sample_particle(N,1.5*ones(N,1),0.9*ones(N,1),3*tick_size*ones(N,1),sigma_a, sigma_b, sigma_mu);

tick_size = 1e-5;
prior_alpha = dirichlet(a,b,c,mu,support_num,tick_size);
weight = zeros(N,1);
for i = 1 : N
    weight(i,1) = log_multinomial(prior_alpha(:,i),bid_side(start_time),support_num,tick_size);
end

%weight normalisation
weight  = weight_normalisation(weight);
% stratified sampling
[a,b,mu,weight ] = stratified_sampling_multi( a,b,mu,weight);
param_map(1,1) = mean(a);
param_map(2,1) = mean(b);
param_map(3,1) = mean(mu);
j = 2;
for t = start_time+1 : end_time
    disp(t)
    [a,b, mu] = sample_particle(N,a,b,mu,sigma_a, sigma_b, sigma_mu);
    weight = zeros(N,1);
    alpha  = dirichlet(a,b,c,mu,support_num,tick_size);
    for i = 1 : N
        weight(i,1) = log_multinomial(alpha(:,i),bid_side(start_time),support_num,tick_size);
    end

    %weight normalisation
    weight  = weight_normalisation(weight);
    % stratified sampling
    [a,b,mu,weight ] = stratified_sampling_multi( a,b,mu,weight);
    param_map(1,j) = mean(a);
    param_map(2,j) = mean(b);
    param_map(3,j) = mean(mu);
    j=j+1;
end


%% plot
figure(3)
i =1;
for t = start_time: end_time
    alpha = dirichlet(param_map(i,1),param_map(i,2),c,param_map(i,3),support_num,tick_size);
    alpha = alpha/sum(alpha);
    max_a = max(alpha);
    for d = 1 : support_num
        %%Construct heat map with position defined by price and time, colour
        %%defined by quantity
    rectangle('Position',[i-1,mid_price_l(t)-d*tick_size,1,tick_size],'Facecolor'...
       ,hsv2rgb([2/3-2/3*alpha(d)/max_a,1.0,1.0]),'edgecolor','none'); 
    end
    i= i+1;
end


