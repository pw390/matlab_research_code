%This file rebuild particle filter in Lin Li's paper
%% ReadIn Data
filename = 'SNP_2.csv';
tick_size = 0.0001;
[bid_side, ask_side, mid_price_l ] = Data_parse(filename,1e8,tick_size);
%% Paramters

Transition_type = 1 ;% 1 for Gamma 2 for inverse Gamma
Observation_type = 1 ;% 1 for Gamma 2 for inverse Gamma

N = 500; % particle number used in sampling and resampling
T = 50; % total time point of simulation
A_intial = 1; % shape parameter of gamma distribution
B_initial = 1; % scale parameter of gamma distribution
% parameter map to store mean of alpha beta s
param_map = zeros(3,T);

% predefined variance
sigma_alpha = 1;
sigma_beta = 1;
sigma_s = 5;

%% Initialise
alpha_particles = gamrnd(A_intial, B_initial, N, 1); % generate alpha
beta_particles = gamrnd(A_intial, B_initial, N, 1); % generate beta
s_particles = zeros(N,1); % generate shift
x = struct('alpha',num2cell(alpha_particles),...
        'beta',num2cell(beta_particles),'s',num2cell(s_particles));
% calculate weight for each particle
weight = zeros(N,1);
for i = 1 : N
    weight(i,1) = log_gamma(bid_side(1), x(i));
end
%% normalisation and resampling
%weight normalisation
weight  = weight_normalisation(weight);
% resampling
[x, weight] = stratified_resampling(x,weight);
% get mean of all particles 
param_map(1,1) = mean([x.alpha]);
param_map(2,1) = mean([x.beta]);
param_map(3,1) = mean([x.s]);
%% Propagation
for t = 2 :T
    disp(t)
    for i = 1 : N
      % Get new x without rejection sampling
      x(i).alpha = gamrnd(x(i).alpha^2/(sigma_alpha^2),sigma_alpha^2/x(i).alpha);
      x(i).beta = gamrnd(x(i).beta^2/(sigma_beta^2),sigma_beta^2/x(i).beta);
      x(i).s = normrnd(x(i).s, sigma_s^2);
      weight(i,1) = log_gamma(bid_side(t), x(i));
    end
    weight = weight_normalisation(weight);
    [x , weight] = stratified_resampling(x, weight);
    % get mean of all particles
    param_map(1,t) = mean([x.alpha]);
    param_map(2,t) = mean([x.beta]);
    param_map(3,t) = mean([x.s]);
end

%% abstract process
ask_param_map = particle_filter(N,T,ask_side);
bid_param_map = particle_filter(N,T,bid_side);
%% plot
tick_size = 0.0001;
plot_heat_map( ask_param_map,bid_param_map,T, depth,mid_price_l,tick_size)
 % bidask 1 for bid -1 for ask
 plot(mid_price_l(1:T))
%%
depth = 20;
tick_size = 0.00001;
for j=1:T
    temp_space = (1:depth);
    temp_color = gampdf(temp_space+param_map(3,j),param_map(1,j),param_map(2,j));
    temp_max = max(temp_color);
    for d = 1 : depth
        %%Construct heat map with position defined by price and time, colour
        %%defined by quantity
    rectangle('Position',[j-1,mid_price_l(j)-d*tick_size,1,tick_size ],'Facecolor'...
       ,hsv2rgb([2/3-2/3*temp_color(d)/temp_max,1.0,1.0]),'edgecolor','none'); 
    end
end
hold on 

plot(mid_price_l(1:T))




