%To repeat the same process of Lin's to get 
filename = 'SNP_3.csv';
[bid_side, ask_side, mid_price_l, Time] = Data_parse(filename,5e4,1);

%% alpha-stable distribution with four parameter
sigma_a = 0.3/10;
sigma_b = 0.2/10;
sigma_mu =1e-5;
sigma_c = 1e-5;

N = 500; % particle number
start_time = 700; % start_time
end_time =800; % end_time
T_duration = end_time - start_time+1;
tick_size = 1e-5;
Truncate = 0;% 0 not truncate >0 for truncate 

bid_parameter = [ones(N,1),0.8*ones(N,1),5e-5*ones(N,1),3*tick_size*ones(N,1)];
ask_parameter = [ones(N,1),0.8*ones(N,1),5e-5*ones(N,1),3*tick_size*ones(N,1)];


bid_param_map = zeros(4,T_duration);
ask_param_map = zeros(4,T_duration);
ask_var_map = zeros(4,T_duration);
bid_var_map = zeros(4,T_duration);
bid_weight_map = zeros(N,T_duration);
ask_weight_map = zeros(N,T_duration);

if Truncate>0
    for i = 1:length(ask_side)
        t_a= find(ask_side(i).abs_diff <=Truncate*tick_size);
        t_b= find(bid_side(i).abs_diff <=Truncate*tick_size);        
        ask_side(i).abs_diff = ask_side(i).abs_diff(t_a);
        ask_side(i).Q = ask_side(i).Q(t_a);
        bid_side(i).abs_diff = bid_side(i).abs_diff(t_b);
        bid_side(i).Q = bid_side(i).Q(t_b);
    end
end
%% bid side
j = 1;
weight = zeros(N,1);
for t = start_time : end_time
    disp(t)
    %sample 
    [bid_parameter(:,1),bid_parameter(:,2),bid_parameter(:,3),bid_parameter(:,4)]...
        = sample_alpha_four_v(...
          bid_parameter(:,1),bid_parameter(:,2),bid_parameter(:,3)...
              ,bid_parameter(:,4),sigma_a, sigma_b, sigma_c,sigma_mu );
    for i = 1:N
        weight(i) = sum((bid_side(t).Q').*log(stable_pdf(bid_side(t).abs_diff,bid_parameter(i,:))));
    end
    bid_weight_map(:,j) = weight;
    weight  = weight_normalisation(weight);
    [bid_parameter] = stratified_resampling_parameter(bid_parameter,weight);
    bid_param_map(:,j)= mean(bid_parameter).';
    bid_var_map(:,j)= var(bid_parameter).';
    j = j+1;
end
%% ask side


j = 1;
weight = zeros(N,1);
for t = start_time : end_time
    disp(t)
    %sample 
    [ask_parameter(:,1),ask_parameter(:,2),ask_parameter(:,3),ask_parameter(:,4)]...
        = sample_alpha_four_v(...
          ask_parameter(:,1),ask_parameter(:,2),ask_parameter(:,3)...
              ,ask_parameter(:,4),sigma_a, sigma_b, sigma_c,sigma_mu );
    for i = 1:N
        weight(i) = sum((ask_side(t).Q').*log(stable_pdf(ask_side(t).abs_diff,ask_parameter(i,:))));
    end
    ask_weight_map(:,j) = weight;
    weight  = weight_normalisation(weight);
    [ask_parameter] = stratified_resampling_parameter(ask_parameter,weight);
    ask_param_map(:,j)= mean(ask_parameter).';
    ask_var_map(:,j)= var(ask_parameter).';
    j = j+1;
end
%% log likelihood
weight_map = ask_weight_map;
marginal_ll=0;
s=20;
for i = s: T_duration
    max_weight = max(weight_map(:,i));
    marginal_ll= marginal_ll+max(weight_map(:,i));
    n_weight = exp(weight_map(:,i) - max_weight);
    marginal_ll = marginal_ll+log(sum(n_weight));
end
marginal_ll = marginal_ll - (T_duration+1-s)*log(N);
disp(marginal_ll)

%% plot

figure
i =1;
if Truncate
    support_num = Truncate;
else
    support_num = 40;
end
for t = start_time : end_time
    disp(t)
    balpha = stable_pdf((1:support_num)*tick_size,bid_param_map(:,i));
    %balpha = balpha/sum(balpha);
    temp_max= max(balpha);
    
    salpha = stable_pdf((1:support_num)*tick_size,ask_param_map(:,i));
    %salpha = salpha/sum(salpha);
    temp_max= max(max(salpha),temp_max);
    for d = 1 : support_num
        %%Construct heat map with position defined by price and time, colour
        %%defined by quantity
    rectangle('Position',[Time(t),mid_price_l(t)-d*tick_size,Time(t+1)-Time(t),tick_size],'Facecolor'...
       ,hsv2rgb([2/3-2/3*balpha(d)/temp_max,1.0,1.0]),'edgecolor','none'); 
    end

    for d = 1 : support_num
        %%Construct heat map with position defined by price and time, colour
        %%defined by quantity
    rectangle('Position',[Time(t),mid_price_l(t)+(d-1)*tick_size,Time(t+1)-Time(t),tick_size],'Facecolor'...
       ,hsv2rgb([2/3-2/3*salpha(d)/temp_max,1.0,1.0]),'edgecolor','none'); 
    end
    i= i+1;
end
xlim([Time(start_time),Time(end_time)]);
hold on 
plot(Time(start_time:end_time),mid_price_l(start_time:end_time),'-','color','k','LineWidth',2);legend('mid price');
ylabel('Price [USD/EUR]');xlabel('Time');
title('Heat map of orderbook rebuilt based on \alpha-stable distribution (reference price: mid-price)');
colorbar();caxis([0 1]);
%set(gca,'color',hsv2rgb([2/3 1 1]));


%% plot with quantity
figure
i =1;
if Truncate
    support_num = Truncate;
else
    support_num = 40;
end
for t = start_time : end_time
    disp(t)
    balpha = stable_pdf((1:support_num)*tick_size,bid_param_map(:,i));
    balpha = balpha*sum(bid_side(t).Q);
    temp_max= max(balpha);
    
    salpha = stable_pdf((1:support_num)*tick_size,ask_param_map(:,i));
    salpha = salpha*sum(ask_side(t).Q);
    temp_max= max(max(salpha),temp_max);
    for d = 1 : support_num
        %%Construct heat map with position defined by price and time, colour
        %%defined by quantity
    rectangle('Position',[Time(t),mid_price_l(t)-d*tick_size,Time(t+1)-Time(t),tick_size],'Facecolor'...
       ,hsv2rgb([2/3-2/3*balpha(d)/temp_max,1.0,1.0]),'edgecolor','none'); 
    end

    for d = 1 : support_num
        %%Construct heat map with position defined by price and time, colour
        %%defined by quantity
    rectangle('Position',[Time(t),mid_price_l(t)+(d-1)*tick_size,Time(t+1)-Time(t),tick_size],'Facecolor'...
       ,hsv2rgb([2/3-2/3*salpha(d)/temp_max,1.0,1.0]),'edgecolor','none'); 
    end
    i= i+1;
end
xlim([Time(start_time),Time(end_time)]);
hold on 
plot(Time(start_time:end_time),mid_price_l(start_time:end_time),'-','color','k','LineWidth',2);legend('mid price');
ylabel('Price [USD/EUR]');xlabel('Time');
title('Heat map of orderbook rebuilt based on \alpha-stable distribution with Quantity (reference price: mid-price)');
colorbar();caxis([0 1]);
%set(gca,'color',hsv2rgb([2/3 1 1]));




%% curve fitting for alpha stable
figure
plot_time = 785;
area_bid = tick_size*dot(ones(numel(bid_side(plot_time).Q),1),bid_side(plot_time).Q);
area_ask = tick_size*dot(ones(numel(ask_side(plot_time).Q),1),ask_side(plot_time).Q);
location = [mid_price_l(plot_time)-bid_side(plot_time).abs_diff;...
        mid_price_l(plot_time)+ask_side(plot_time).abs_diff];
quantity = round([bid_side(plot_time).Q /area_bid; ask_side(plot_time).Q/area_ask] );
ylabel('Normalised Order Qty');
%yyaxis left
bar(location,quantity,'Facecolor',[0.9 0.9 0.9],'edgecolor',[0.9 0.9 0.9]);
hold on 
disp(1)
b_axis =0:tick_size:max(bid_side(plot_time).abs_diff+10*tick_size);
a_axis =0:tick_size:max(ask_side(plot_time).abs_diff+10*tick_size);
bid_p = stable_pdf(b_axis,bid_param_map(:,plot_time-start_time+1));
ask_p = stable_pdf(a_axis,ask_param_map(:,plot_time-start_time+1));
%yyaxis right
plot(mid_price_l(plot_time)-fliplr(b_axis),fliplr(bid_p),'LineWidth',2);
plot(mid_price_l(plot_time)+(a_axis),(ask_p),'LineWidth',2);
xlim([min(location)-10*tick_size,max(location)+10*tick_size]);
xlabel('Price [USD/EUR]');

set(gca,'color',[0.7 0.7 0.7]);

%% plot parameter
figure;

subplot(4,1,1);

filter_range =Time(start_time:end_time);
param_map = bid_param_map;
var_map = bid_var_map;
alpha_est = param_map(1,:);
alpha_std = sqrt(var_map(1,:));
beta_est = param_map(2,:);
beta_std = sqrt(var_map(2,:));
c_est = param_map(3,:);
c_std = sqrt(var_map(3,:));
mu_est = param_map(3,:);
mu_std = sqrt(var_map(3,:));
plot(filter_range,alpha_est,'red');
errorbar(filter_range,alpha_est,alpha_std,'red');
xlabel('Time point');xlim([Time(start_time) Time(end_time)]);
ylabel('Stability');

subplot(4,1,2);
plot(filter_range,beta_est,'red');
errorbar(filter_range,beta_est,beta_std,'red');
xlabel('Time point');xlim([Time(start_time) Time(end_time)]);
ylabel('Skewness');

subplot(4,1,3);
plot(filter_range,c_est,'red');
errorbar(filter_range,c_est,c_std,'red');
xlabel('Time point');xlim([Time(start_time) Time(end_time)]);
ylabel('Scale');

subplot(4,1,4);
plot(filter_range,mu_est,'red');
errorbar(filter_range,mu_est,mu_std,'red');
xlabel('Time point');xlim([Time(start_time) Time(end_time)]);
ylabel('Scale');
title('BID')









