function [weight] = log_both_side( bid_parameter,ask_parameter,c,...
        price_state,bid_side, ask_side,support_num,below_base,tick_size,N)
bid_alpha = bin_pro(bid_parameter(:,1),...
    bid_parameter(:,2),c,bid_parameter(:,3),support_num,below_base,tick_size);
ask_alpha = bin_pro(ask_parameter(:,1),...
    ask_parameter(:,2),c,ask_parameter(:,3),support_num,below_base,tick_size);
weight = zeros(N,1);
for i = 1 : N
    bid_hist = zeros(support_num,1);
    ask_hist = zeros(support_num,1);
    for j = 1 : length(bid_side.price)
        ind = floor((-bid_side.price(j)+price_state(i,1))/tick_size)+below_base+1;
        if ind <= support_num && ind > 0 
            bid_hist(ind,1) = bid_hist(ind,1) + round(bid_side.Q(j));
        end
    end
    
    for j = 1 : length(ask_side.price)
        ind = floor((ask_side.price(j)-price_state(i,1))/tick_size)+below_base+1;
        if ind <= support_num && ind > 0 
            ask_hist(ind,1) = ask_hist(ind,1) + round(ask_side.Q(j));
        end
    end
    factorial_bid = log_fac(sum(bid_hist))-sum(log_fac(bid_hist));
    factorial_ask = log_fac(sum(ask_hist))-sum(log_fac(ask_hist));
    weight(i,1)= factorial_bid+factorial_ask+sum(bid_hist.*log(bid_alpha(:,i))) + sum(ask_hist.*log(ask_alpha(:,i)));
    
end

end

