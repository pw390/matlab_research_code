%% This file is aimed to prove the approximation is valid
% range of a is from 0.5 to 1.5
% range of b is 0.6 to 1
% c is 5e-5
% range of mu doesn't matter but I assume mu for 1.296 - 1.2968 
% x range from (-below_base+0.5:1:support_num-below_base-0.5)*tick_size;
support_num = 80;
below_base = 40;
tick_size =1e-5;
x = (-below_base+1:1:support_num-below_base)*tick_size;
y = (-below_base+0.5:1:support_num-below_base+0.5)*tick_size;
mu = 0;
c = 5e-5;
a = 0.5 : 0.02:1.5;
b = 0.6 : 0.02 : 1;
m = size(a,2);
n = size(b,2);
[A ,B ] = meshgrid(a,b);
A_v = reshape(A,m*n,1);
B_v = reshape(B,m*n,1);
KL_v = 0*A_v;
for i = 1:m*n
    pars=[A_v(i,1),B_v(i,1),c,mu];
    pdf_1 = stable_pdf(x, pars);
    pdf_1 = pdf_1/sum(pdf_1);
    cdf_1 = stable_cdf(y,pars);
    cdf_1 = cdf_1(2:end) - cdf_1(1:end-1);
    cdf_1 = cdf_1/sum(cdf_1);
    
    % find both nonzero term
    k = pdf_1 .* cdf_1;
    ind = find(k);
    pdf_1 = pdf_1(ind);
    cdf_1 = cdf_1(ind);
    KL_v(i,1)= sum(pdf_1.* (log(pdf_1)-log(cdf_1)));
end
figure
KL_v(isnan(KL_v))=0;
KL_v = reshape(KL_v,n,m);
surf(A,B,real(KL_v))
