function [ log_ll] = log_gamma_hist( pdist, shift,bid_side,support_num,tick_size)
    hist_count= zeros(support_num,1);
    for j = 1 : length(bid_side.abs_diff)
       ind = floor((bid_side.abs_diff(j)+shift)/tick_size)+1;
       if ind <=support_num && ind>=1
       hist_count(ind,1) = hist_count(ind,1) + round(bid_side.Q(j));
       end
    end
    log_ll =  log_fac(sum(hist_count))-sum(log_fac(hist_count))+sum(hist_count.* log(pdist));
    
end

