%% review the paper about kalman filter 
filename = 'SNP_3.csv';
[bid_side, ask_side, mid_price_l, Time] = Data_parse(filename,5e4,1);
%% truncate
tick_size = 1e-5;
Truncate = 0;% 0 not truncate >0 for truncate 
if Truncate>0
    for i = 1:length(ask_side)
        t_a= find(ask_side(i).abs_diff <=Truncate*tick_size);
        t_b= find(bid_side(i).abs_diff <=Truncate*tick_size);        
        ask_side(i).abs_diff = ask_side(i).abs_diff(t_a);
        ask_side(i).Q = ask_side(i).Q(t_a);
        bid_side(i).abs_diff = bid_side(i).abs_diff(t_b);
        bid_side(i).Q = bid_side(i).Q(t_b);
    end
end
%% gamfit

start_time = 700;
end_time = 800;

T_duration = end_time - start_time + 1;
bid_parameter = zeros(2,T_duration);
ask_parameter = zeros(2,T_duration);

%% get est alpha beta
i = 1;
for t = start_time : end_time
     phi = gamfit(bid_side(t).abs_diff,[],[],bid_side(t).Q);
     bid_parameter(:,i) = phi';
     phi= gamfit(ask_side(t).abs_diff,[],[],ask_side(t).Q); 
    ask_parameter(:,i) = phi'; 
    i = i+1;
end

%% kalman smoother implementation
Q = 0.1;
R = 0.1;
A = 1;
B = 0;
H = 1;
%% initialization
z = bid_parameter(1,:);
u =0*x;
x = 0* bid_parameter(1,:);
P = 0* bid_parameter(1,:);
P(1) = Q;
x(1) = bid_parameter(1,1);

%% forward
for  i = 2:T_duration

    %predict
    x(i) = A * x(i-1) + B*u(i);
    P(i) = A * P(i-1) * A'+Q;
    %update
    
    y =  z(i)- H*x(i);
    K = P(i)*H'/(H*P(i)*H'+R);
    x(i) = x(i) + K *y;
    P(i) = P(i) - K * H * P(i);

end

%% backward
x_sm = 0* bid_parameter(1,:);
P_sm = 0* bid_parameter(1,:);
P_sm (T_duration) = P(T_duration);
x_sm(T_duration) = x(T_duration);
for i = fliplr(2:T_duration-1)
   L = P(i) * A' /(P(i+1)) ;
   x_sm (i)= x(i) + L*(x_sm(i+1)-x(i+1));
   P_sm(i) = P(i) + L*(P_sm(i+1)-P(i+1))*L';

end
x_sm(1)=x(1);
%% plot alpha beta
figure
subplot(2,2,1)
Q = 1/10000;
R = 0.1;
A = 1;
B = 0;
H = 1;
z = bid_parameter(1,:);
u = 0 * z;
plot(bid_parameter(1,:));
hold on
[x_sm ] = kalman_smoother(z,u,A,B,H,Q,R );
plot(x_sm,'-.','LineWidth',1.5)
hold on
[x_sm ] = kalman_smoother(z,u,A,B,H,Q*100,R );
plot(x_sm,'g--','LineWidth',1.5)
hold on
b_alpha_sm = x_sm;
%lgd =legend('gamfit','smoothed small Q','smoothed large Q');
title('bid side \alpha','FontSize',15)
xlim([0,100])
subplot(2,2,2)
Q = 1e-12;
R = 100*2e-12;
A = 1;
B = 0;
H = 1;
z = bid_parameter(2,:);
u = 0 * z;
plot(bid_parameter(2,:));
hold on
[x_sm ] = kalman_smoother(z,u,A,B,H,Q,R );
plot(x_sm,'-.','LineWidth',1.5)
hold on
[x_sm ] = kalman_smoother(z,u,A,B,H,Q*100,R );
plot(x_sm,'g--','LineWidth',1.5)
hold on
title('bid side \beta','FontSize',15)
xlim([0,100])
b_beta_sm = x_sm;
subplot(2,2,3)
Q = 1/10000;
R = 0.1;
A = 1;
B = 0;
H = 1;
z = ask_parameter(1,:);
u = 0 * z;
plot(ask_parameter(1,:))
hold on
[x_sm ] = kalman_smoother(z,u,A,B,H,Q,R );
plot(x_sm,'-.','LineWidth',1.5)
hold on
[x_sm ] = kalman_smoother(z,u,A,B,H,Q*100,R );
plot(x_sm,'g--','LineWidth',1.5)
hold on
s_alpha_sm = x_sm;
%lgd =legend('gamfit','smoothed small Q','smoothed large Q');

title('ask side \alpha','FontSize',15)
xlim([0,100])
subplot(2,2,4)
Q = 1e-12;
R = 100*2e-12;
A = 1;
B = 0;
H = 1;
z = ask_parameter(2,:);
u = 0 * z;
plot(ask_parameter(2,:));
hold on
[x_sm ] = kalman_smoother(z,u,A,B,H,Q,R );
plot(x_sm,'-.','LineWidth',1.5)
hold on
[x_sm ] = kalman_smoother(z,u,A,B,H,Q*100,R );
plot(x_sm,'g--','LineWidth',1.5)
hold on
s_beta_sm = x_sm;
%lgd =legend('gamfit','smoothed small Q','smoothed large Q');

title('ask side \beta','FontSize',15)
xlim([0,100])
%% plot heat map with smoothed alpah and beta
figure
i =1;
support_num = 60;
for t = start_time : end_time
    disp(t)
    x = (1:support_num)*tick_size;
    balpha = gampdf(x, b_alpha_sm(i),b_beta_sm(i)); 
    temp_max= max(balpha);
    
    salpha = gampdf(x, s_alpha_sm(i),s_beta_sm(i)); 
    temp_max= max(max(salpha),temp_max);
    for d = 1 : support_num
        %%Construct heat map with position defined by price and time, colour
        %%defined by quantity
    rectangle('Position',[Time(t),mid_price_l(t)-d*tick_size,Time(t+1)-Time(t),tick_size],'Facecolor'...
       ,hsv2rgb([2/3-2/3*balpha(d)/temp_max,1.0,1.0]),'edgecolor','none'); 
    end

    for d = 1 : support_num
        %%Construct heat map with position defined by price and time, colour
        %%defined by quantity
    rectangle('Position',[Time(t),mid_price_l(t)+(d-1)*tick_size,Time(t+1)-Time(t),tick_size],'Facecolor'...
       ,hsv2rgb([2/3-2/3*salpha(d)/temp_max,1.0,1.0]),'edgecolor','none'); 
    end
    i= i+1;
end
xlim([Time(start_time),Time(end_time)]);
hold on 
plot(Time(start_time:end_time),mid_price_l(start_time:end_time),'-','color','k','LineWidth',2);legend('mid price');
ylabel('Price [USD/EUR]');xlabel('Time');
title('Heat map of orderbook rebuilt based on Gamma distribution with Quantity (reference price: mid-price)');
colorbar();caxis([0 1]);
%% curve fitting

figure
plot_time = 746;
Truncate = 60;
if Truncate>0
        t_a= find(ask_side(plot_time).abs_diff <=Truncate*tick_size);
        t_b= find(bid_side(plot_time).abs_diff <=Truncate*tick_size);        
        ask_side(plot_time).abs_diff = ask_side(plot_time).abs_diff(t_a);
        ask_side(plot_time).Q = ask_side(plot_time).Q(t_a);
        bid_side(plot_time).abs_diff = bid_side(plot_time).abs_diff(t_b);
        bid_side(plot_time).Q = bid_side(plot_time).Q(t_b);
end
area_bid =  sum(bid_side(plot_time).Q)*tick_size;
area_ask = sum(ask_side(plot_time).Q)*tick_size;
location = [mid_price_l(plot_time)-bid_side(plot_time).abs_diff]
quantity = round([bid_side(plot_time).Q]);
ylabel('Normalised Order Qty');
z1=bar(location,quantity,'Facecolor','c','edgecolor','none');
quantity = round( ask_side(plot_time).Q);
hold on 
location =[mid_price_l(plot_time)+ask_side(plot_time).abs_diff];
z2=bar(location,quantity,'Facecolor','r','edgecolor','none'); 
pt= plot_time - start_time + 1;
x= (1:Truncate)*tick_size;
b_axis =x;
a_axis =x;
cdf1 = gampdf(x, b_alpha_sm(pt),b_beta_sm(pt)); 
bid_p = cdf1;
cdf1 = gampdf(x, b_alpha_sm(pt),s_beta_sm(pt)); 
ask_p = cdf1;
p1=plot(mid_price_l(plot_time)-b_axis,bid_p*area_bid,'k','LineWidth',2)
hold on;
p2=plot(mid_price_l(plot_time)+(a_axis),(ask_p)*area_ask,'k','LineWidth',2);


y1=get(gca,'ylim');
hold on
%title(strcat('Curve fitting at time point \tau=',num2str(plot_time)))
load('inv_gamma_ask');
load('inv_gamma_bid');

pt= plot_time - start_time + 1;
%bid_param_map = inv_gamma_bid.bid_param_map;
%ask_param_map = inv_gamma_ask.ask_param_map;

b_axis = -bid_param_map(3,pt) + x-0.5*tick_size;
a_axis = -ask_param_map(3,pt) + x-0.5*tick_size;
cdf1 = gammainc(bid_param_map(2,pt)./x,bid_param_map(1,pt),'upper'); 
balpha = (cdf1-[0,cdf1(1:end-1)])';
bid_p = balpha/sum(balpha)*sum(bid_side(plot_time).Q);
cdf1 = gammainc(ask_param_map(2,pt)./x,ask_param_map(1,pt),'upper');
balpha = (cdf1-[0,cdf1(1:end-1)])';
ask_p = balpha/sum(balpha)*sum(ask_side(plot_time).Q);
p3=plot(mid_price_l(plot_time)-b_axis,bid_p,'g-^','LineWidth',1);
hold on;
p4=plot(mid_price_l(plot_time)+(a_axis),(ask_p),'g-^','LineWidth',1);


load ('gam_ask_param');
load('gam_bid_param');
%bid_param_map = gamma_bid.bid_param_map;
%ask_param_map = gamma_ask.ask_param_map;
b_axis = -bid_param_map(3,pt) + x-0.5*tick_size;
a_axis = -ask_param_map(3,pt) + x-0.5*tick_size;
cdf1 = gamcdf(x, bid_param_map(1,pt),bid_param_map(2,pt)); 
balpha = (cdf1-[0,cdf1(1:end-1)])';
bid_p = balpha/sum(balpha)*sum(bid_side(plot_time).Q);
cdf1 = gamcdf(x, ask_param_map(1,pt),ask_param_map(2,pt)); 
balpha = (cdf1-[0,cdf1(1:end-1)])';
ask_p = balpha/sum(balpha)*sum(ask_side(plot_time).Q);
p5=plot(mid_price_l(plot_time)-b_axis,bid_p,'b-o','LineWidth',2);
hold on;
p6=plot(mid_price_l(plot_time)+(a_axis),(ask_p),'b-o','LineWidth',2);
lgd=legend([z1,z2],'bid orders','ask orders')
lgd.FontSize = 20
ylabel('Volume','FontSize',20);
xlabel('Price [USD/EUR]','FontSize',20);