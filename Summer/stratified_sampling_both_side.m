function [new_bid, new_ask, new_price] = stratified_sampling_both_side(...
                bid_parameter,ask_parameter,price_state,weight)
new_bid = 0*bid_parameter;
new_ask = 0*ask_parameter;
new_price = 0*price_state;
N = size(bid_parameter,1);
c = cumsum(weight);
for r =1 : N
    u  = (rand()+(r-1))/N;
    i = find((c >= u),1);
    new_bid(r,:) = bid_parameter(i,:);
    new_ask(r,:) = ask_parameter(i,:);
    new_price(r,:) = price_state(i,:);
end



end

