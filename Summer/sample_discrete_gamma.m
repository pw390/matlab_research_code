function [new_alpha, new_beta,new_shift, pdist] = sample_discrete_gamma(support_num, sigma_shape, sigma_scale,sigma_shift,...
    old_alpha, old_beta,old_shift,tick_size,minimum)
N = length(old_alpha);
pdist = zeros(support_num, N);
new_alpha= zeros(N,1);
new_beta= zeros(N,1);
new_shift= zeros(N,1);
for i = 1 : N
    while sum(pdist(:,i)==0)>0
        new_alpha(i) = gamrnd(old_alpha(i)^2/sigma_shape^2,sigma_shape^2/old_alpha(i));
        while new_alpha(i)<=1.3
            new_alpha(i) = gamrnd(old_alpha(i)^2/sigma_shape^2,sigma_shape^2/old_alpha(i));
        end
        new_beta(i) = gamrnd(old_beta(i)^2/sigma_scale^2,sigma_scale^2/old_beta(i));
        new_shift(i)= normrnd(old_shift(i),sigma_shift);
        while (new_shift(i)+minimum)<=0 || new_shift(i)>0
            new_shift(i)= normrnd(old_shift(i),sigma_shift);
        end
        x = (1:support_num)*tick_size;
        cdf1 = gamcdf(x, new_alpha(i),new_beta(i)); 
        pdist(:,i) = (cdf1-[0,cdf1(1:end-1)])';
        pdist(:,i)= pdist(:,i)/sum(pdist(:,i));
    end
end


end

