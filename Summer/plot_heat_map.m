function plot_heat_map( ask_param_map,bid_param_map,T, depth,mid_price_l,tick_size)
 % bid side first
 param_map = bid_param_map;
    for j=1:T
        temp_space = (1:depth);
        temp_color = gampdf(temp_space+param_map(3,j),param_map(1,j),param_map(2,j));
        temp_max = max(temp_color);
        for d = 1 : depth
        %%Construct heat map with position defined by price and time, colour
        %%defined by quantity
        rectangle('Position',[j-1,mid_price_l(j)-d*tick_size,1,tick_size ],'Facecolor'...
       ,hsv2rgb([2/3-2/3*temp_color(d)/temp_max,1.0,1.0]),'edgecolor','none'); 
        end
    end
  param_map = ask_param_map;
    for j=1:T
        temp_space = (1:depth);
        temp_color = gampdf(temp_space+param_map(3,j),param_map(1,j),param_map(2,j));
        temp_max = max(temp_color);
        for d = 1 : depth
        %%Construct heat map with position defined by price and time, colour
        %%defined by quantity
        rectangle('Position',[j-1,mid_price_l(1)+(d-1)*tick_size,1,tick_size ],'Facecolor'...
       ,hsv2rgb([2/3-2/3*temp_color(d)/temp_max,1.0,1.0]),'edgecolor','none'); 
        end
    end
hold on 
end

