function [ new_parameter ] = stratified_resampling_parameter( parameter,weight)
new_parameter =  0 * parameter;
N = size(parameter,1);
c = cumsum(weight);
for r =1 : N
    u  = (rand()+(r-1))/N;
    i = find((c >= u),1);
    new_parameter(r,:) = parameter(i,:);
end

end

