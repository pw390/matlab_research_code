%% ReadIn Data
filename = 'SNP_3.csv';
[bid_side, ask_side, mid_price_l, Time] = Data_parse(filename,5e4,1);


%% parameter
sigma_a = 0.3;
sigma_b = 0.2;
sigma_mu = 20e-5;

N = 100; % particle number
start_time = 301; % start_time
end_time =450; % end_time
T_duration = end_time - start_time+1;
support_num = 40; % support number of multinomial distribution
bid_param_map = zeros(3,T_duration);
ask_param_map = zeros(3,T_duration);
Quantity_bid = zeros(T_duration,1);
Quantity_ask = zeros(T_duration,1);
c = 5e-5;
tick_size = 1e-5;

%% bid
[a,b, mu] = sample_particle(N,ones(N,1),0.8*ones(N,1),3*tick_size*ones(N,1),sigma_a, sigma_b, sigma_mu);
tick_size = 1e-5;
prior_alpha = dirichlet(a,b,c,mu,support_num,tick_size);
weight = zeros(N,1);
Quantity_bid(1,1) = sum(histogram(bid_side(start_time),support_num,tick_size));
for i = 1 : N
    weight(i,1) = log_hist(prior_alpha(:,i),bid_side(start_time),support_num,tick_size);
end 

%weight normalisation
weight  = weight_normalisation(weight);
% stratified sampling
[a,b,mu,weight ] = stratified_sampling_multi( a,b,mu,weight);
bid_param_map(1,1) = mean(a);
bid_param_map(2,1) = mean(b);
bid_param_map(3,1) = mean(mu);
j = 2;
for t = start_time+1: end_time
    disp(t)
    [a,b, mu] = sample_particle(N,a,b,mu,sigma_a, sigma_b, sigma_mu);
    weight = zeros(N,1);
    alpha  = dirichlet(a,b,c,mu,support_num,tick_size);
    for i = 1 : N
        weight(i,1) = log_hist(alpha(:,i),bid_side(t),support_num,tick_size);
    end
    Quantity_bid(j,1) = sum(histogram(bid_side(t),support_num,tick_size));
    %weight normalisation
    weight  = weight_normalisation(weight);
    % stratified sampling
    [a,b,mu,weight ] = stratified_sampling_multi( a,b,mu,weight);
    bid_param_map(1,j) = mean(a);
    bid_param_map(2,j) = mean(b);
    bid_param_map(3,j) = mean(mu);
    j=j+1;
end

[a,b, mu] = sample_particle(N,ones(N,1),0.8*ones(N,1),3*tick_size*ones(N,1),sigma_a, sigma_b, sigma_mu);

prior_alpha = dirichlet(a,b,c,mu,support_num,tick_size);
weight = zeros(N,1);
for i = 1 : N
    weight(i,1) = log_hist(prior_alpha(:,i),ask_side(start_time),support_num,tick_size);
end
Quantity_ask(1,1) = sum(histogram(ask_side(start_time),support_num,tick_size));
%weight normalisation
weight  = weight_normalisation(weight);
% stratified sampling
[a,b,mu,weight ] = stratified_sampling_multi( a,b,mu,weight);
ask_param_map(1,1) = mean(a);
ask_param_map(2,1) = mean(b);
ask_param_map(3,1) = mean(mu);
j = 2;
for t = start_time+1 : end_time
    disp(t)
    [a,b, mu] = sample_particle(N,a,b,mu,sigma_a, sigma_b, sigma_mu);
    weight = zeros(N,1);
    alpha  = dirichlet(a,b,c,mu,support_num,tick_size);
    for i = 1 : N
        weight(i,1) = log_hist(alpha(:,i),ask_side(t),support_num,tick_size);
    end
    Quantity_ask(j,1) = sum(histogram(ask_side(t),support_num,tick_size));
    %weight normalisation
    weight  = weight_normalisation(weight);
    % stratified sampling
    [a,b,mu,weight ] = stratified_sampling_multi( a,b,mu,weight);
    ask_param_map(1,j) = mean(a);
    ask_param_map(2,j) = mean(b);
    ask_param_map(3,j) = mean(mu);
    j=j+1;
end
%% plot method 1
i =1;
for t = start_time : end_time
    disp(t)
    balpha = dirichlet(bid_param_map(1,i),bid_param_map(2,i),c,bid_param_map(3,i),support_num,tick_size);
    balpha = balpha/sum(balpha);
    temp_max= max(balpha);
    for d = 1 : support_num
        %%Construct heat map with position defined by price and time, colour
        %%defined by quantity
    rectangle('Position',[i-1,mid_price_l(t)-d*tick_size,1,tick_size],'Facecolor'...
       ,hsv2rgb([2/3-2/3*balpha(d)/temp_max,1.0,1.0]),'edgecolor','none'); 
    end
    salpha = dirichlet(ask_param_map(1,i),ask_param_map(2,i),c,ask_param_map(3,i),support_num,tick_size);
    salpha = salpha/sum(salpha);
    temp_max= max(salpha);


    for d = 1 : support_num
        %%Construct heat map with position defined by price and time, colour
        %%defined by quantity
    rectangle('Position',[i-1,mid_price_l(t)+(d-1)*tick_size,1,tick_size],'Facecolor'...
       ,hsv2rgb([2/3-2/3*salpha(d)/temp_max,1.0,1.0]),'edgecolor','none'); 
    end
    i= i+1;
end

%% plot method 2
figure
i =1;
for t = start_time : end_time
    disp(t)
    balpha = dirichlet(bid_param_map(1,i),bid_param_map(2,i),c,bid_param_map(3,i),support_num,tick_size);
    balpha = balpha/sum(balpha);
    temp_max= max(balpha);
    
    salpha = dirichlet(ask_param_map(1,i),ask_param_map(2,i),c,ask_param_map(3,i),support_num,tick_size);
    salpha = salpha/sum(salpha);
    temp_max= max(max(salpha),temp_max);
    for d = 1 : support_num
        %%Construct heat map with position defined by price and time, colour
        %%defined by quantity
    rectangle('Position',[i-1,mid_price_l(t)-d*tick_size,1,tick_size],'Facecolor'...
       ,hsv2rgb([2/3-2/3*balpha(d)/temp_max,1.0,1.0]),'edgecolor','none'); 
    end

    for d = 1 : support_num
        %%Construct heat map with position defined by price and time, colour
        %%defined by quantity
    rectangle('Position',[i-1,mid_price_l(t)+(d-1)*tick_size,1,tick_size],'Facecolor'...
       ,hsv2rgb([2/3-2/3*salpha(d)/temp_max,1.0,1.0]),'edgecolor','none'); 
    end
    i= i+1;
end
%% plot method 3
figure
i =1;

for t = start_time : end_time
    disp(t)
    Quantity_ask(i,1) = sum(histogram(ask_side(t),support_num,tick_size));
    Quantity_bid(i,1) = sum(histogram(bid_side(t),support_num,tick_size));
    balpha = dirichlet(bid_param_map(1,i),bid_param_map(2,i),c,bid_param_map(3,i),support_num,tick_size);
    balpha = balpha/sum(balpha)*Quantity_bid(i,1);
    temp_max= max(balpha);
    
    salpha = dirichlet(ask_param_map(1,i),ask_param_map(2,i),c,ask_param_map(3,i),support_num,tick_size);
    salpha = salpha/sum(salpha)*Quantity_ask(i,1);
    temp_max= max(max(salpha),temp_max);
    for d = 1 : support_num
        %%Construct heat map with position defined by price and time, colour
        %%defined by quantity
    rectangle('Position',[i-1,mid_price_l(t)-d*tick_size,1,tick_size],'Facecolor'...
       ,hsv2rgb([2/3-2/3*balpha(d)/temp_max,1.0,1.0]),'edgecolor','none'); 
    end

    for d = 1 : support_num
        %%Construct heat map with position defined by price and time, colour
        %%defined by quantity
    rectangle('Position',[i-1,mid_price_l(t)+(d-1)*tick_size,1,tick_size],'Facecolor'...
       ,hsv2rgb([2/3-2/3*salpha(d)/temp_max,1.0,1.0]),'edgecolor','none'); 
    end
    i= i+1;
end  
