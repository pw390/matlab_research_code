%% stable
tic
pd1 = makedist('Stable','alpha',1.1,'beta',0.8,'gam',5e-5,'delta',10e-5);


x = -20e-5:1e-5:40*1e-5;
pdf1 = pdf(pd1,x);
toc

pars=[1.1,0.8,5e-5,1e-5];
pdf_1 = stable_pdf(x, pars);
figure
plot(x,pdf1,'b-');
hold on
plot(x,pdf_1,'rx');
%% gamma
figure
tic;
a = 12;
sigma_a = 4;
x = 1:40;
cdf1 = gamcdf(x,a^2/sigma_a^2,sigma_a^2/a);
cdf2 = [0,cdf1(1:end-1)]; 
pdist = (cdf1-cdf2)';
pdist= pdist/sum(pdist);
toc;
plot(x,pdist,'b-');
hold on
plot(x,gampdf(x,a^2/sigma_a^2,sigma_a^2/a));


%% gamma2
figure
a = 0.5;
b= 1e-5;
x = 1e-5:1e-5:40e-5;
cdf1 = gamcdf(x,a,b);
cdf2 = [0,cdf1(1:end-1)]; 
pdist = (cdf1-cdf2)';
pdist= pdist/sum(pdist);
plot(x,pdist,'r-');
hold on
plot(x,gampdf(x,a,b));o

%% gamma


a = 2; b = 4;
data = gamrnd(a,b,100,1);

[p,ci] = gamfit(data);

plot(data);


%% trial of mid price change
figure
%%
mprice = 1;
sigma_m = 0;
theta=0;
sigma_theta = 0.25e-5;
alpha = -0.5;
t = 5000;
motion_list = zeros(t,1);

for i = 1:t
    motion_list(i)= mprice;
    mprice =mprice +theta+ sigma_m*randn();
    theta = alpha*theta + sigma_theta*randn();
end

plot(motion_list)
hold on
%% plot comparison
support_num = 80;
below_base = 40;
tick_size =1e-5;
mu = 0;
c = 5e-5;
x = (-below_base+1:1:support_num-below_base)*tick_size;
y = (-below_base+0.5:1:support_num-below_base+0.5)*tick_size;

pars=[0.98,0.98,c,mu];
pdf_1 = stable_pdf(x, pars);
pdf_1 = pdf_1/sum(pdf_1);
cdf_1 = stable_cdf(y,pars);
cdf_1 = cdf_1(2:end) - cdf_1(1:end-1);
cdf_1 = cdf_1/sum(cdf_1);

plot(pdf_1)
hold on 


%% figure
figure
beta = 5.8e-5;
alpha = 0.5;
x = (1:support_num)*tick_size;
cdf1 = gammainc(beta./x,alpha,'upper'); 
balpha = (cdf1-[0,cdf1(1:end-1)])';
balpha = balpha/sum(balpha);
plot(x,balpha);
%%
figure
for d = 1 : support_num
        %%Construct heat map with position defined by price and time, colour
        %%defined by quantity
rectangle('Position',[1,1+(d-1)*tick_size,1,tick_size],'Facecolor'...
       ,hsv2rgb([2/3-2/3*d/support_num,1.0,1.0]),'edgecolor','none'); 
 end