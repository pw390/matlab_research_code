function [ result ] = log_fac( x )
result = 0*x;
for i = 1 : length(x)
    if x(i)== 0 
        result(i) = 0;
    else
        result(i)= sum(log(1:x(i)));
    end
end

end

