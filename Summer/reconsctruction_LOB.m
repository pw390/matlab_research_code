%% read data
CurrentDir = pwd;
%Load Snapshot data that includes all price levels
Infile = fullfile(CurrentDir,'Hourly','SNP_2.csv');
fid = fopen(Infile);
Snapshot_Data = textscan(fid,'%f %f %f %f','delimiter',','); fclose(fid);
%Load Best Bid and Offer data that only with best prices
Infile = fullfile(CurrentDir,'Hourly','BBO_2.csv');
fid = fopen(Infile);
BBO_Data = textscan(fid,'%f %f %f %f %f','delimiter',','); fclose(fid);
%Extract each attribute
bbo_t = BBO_Data{1};
bbo_bid = BBO_Data{2};
bbo_ask = BBO_Data{4};
mid_price = (bbo_bid+bbo_ask)/2;
ticksize = 1e-5;
Time = Snapshot_Data{1};
Type = Snapshot_Data{2};
Price_Y = Snapshot_Data{3}; 
Quantity_Z = Snapshot_Data{4};
%% draw
Ti = 50000;
%sort in quantity to determine its colour
Time = Time(1:Ti);
Price_Y = Price_Y(1:Ti);
Quantity_Z = Quantity_Z(1:Ti);
[Zs,ind] = sort(Quantity_Z);
Xs = Time(ind);
Ys = Price_Y(ind);
tic
for j=1:length(Time)
    %%Construct heat map with position defined by price and time, colour
    %%defined by quantity
    rectangle('Position',[Xs(j),Ys(j)-0.00001/2,1.8,0.00001],'Facecolor',...
        hsv2rgb([2/3-(j-1)/(length(ind)*3/2),1.0,1.0]),'edgecolor','none') 
end
xlim([min(Xs),max(Xs)])
hold on
%plot the mid-price curve
%plot(bbo_t,mid_price,'black');