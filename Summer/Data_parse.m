function [ bid_side, ask_side,mid_price_l, unique_time] = Data_parse( filename_1,quantity_base,tick_size)
% first filen name for snp
CurrentDir = pwd;

%Load Snapshot data that includes all price levels
Infile = fullfile(CurrentDir,'Hourly',filename_1);
fid = fopen(Infile);
Snapshot_Data = textscan(fid,'%f %f %f %f','Delimiter',','); 
fclose(fid);

Time = Snapshot_Data{1};
% 1 for bid 2 for ask
Type = Snapshot_Data{2};
Price_Y = Snapshot_Data{3}; 
Quantity_Z = Snapshot_Data{4};

unique_time = unique(Time);
N = length(unique_time);
clear bid_ind;
for i = 1:N
    time_stamp = unique_time(i); 
    t = find(Time == time_stamp);
    bid_ind = t(Type(t)==1);
    bid_price = Price_Y(bid_ind);
    bid_quantity = Quantity_Z(bid_ind)/quantity_base;
    
    ask_ind = t(Type(t)==2);
    ask_price = Price_Y(ask_ind);
    ask_quantity = Quantity_Z(ask_ind)/quantity_base;
    
    if isempty(ask_price) 
        best_ask  = 0;
    else
        best_ask = min(ask_price);
    end
    
    
    if isempty(bid_price)
        best_bid = 0;
    else
        best_bid = max(bid_price);
    end
    
    if best_bid == 0 &&  best_ask == 0
       mid_price= mid_price_l(i-1);
    else
        if best_bid == 0
            mid_price = mid_price_l(i-1);
%             mid_price= best_ask;
        else
            if best_ask == 0
                mid_price = mid_price_l(i-1);
%                 mid_price = best_bid;
            else
                mid_price = (best_ask + best_bid)/2;
            end
        end
    end
    
    
    bid_price = abs(bid_price - mid_price)/tick_size;
    ask_price = abs(ask_price - mid_price)/tick_size;
    
    bid_side(i).abs_diff = flipud(bid_price);
    bid_side(i).Q = flipud(bid_quantity);
    
    ask_side(i).abs_diff = ask_price;
    ask_side(i).Q = ask_quantity;
    
    mid_price_l(i) = mid_price;
    
    clear bid_ind;
end


end

