function [ n_weight ] = weight_normalisation( weight )
% The aim of this function is to normalise the weight

max_weight = max(weight);

n_weight = exp(weight - max_weight);

n_weight = n_weight / sum(n_weight);


end

