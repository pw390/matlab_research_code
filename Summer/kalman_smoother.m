function [x_sm ] = kalman_smoother(z,u,A,B,H,Q,R )
% a kalman smoother
m = size(A,2);
T = size(z,2);
P = Q;
x = zeros(m,T);
x(:,1) = z(:,1);
%% forward
for  i = 2:T

    %predict
    x(:,i) = A * x(:,i-1) + B*u(:,i);
    P(:,:,i) = A * P(:,:,i-1) * A'+Q;
    %update
    
    y =  z(:,i)- H*x(:,i);
    K = P(:,:,i)*H'/(H*P(:,:,i)*H'+R);
    x(:,i) = x(:,i) + K *y;
    P(:,:,i) = P(:,:,i) - K * H * P(:,:,i);

end

%% backward
x_sm = 0* x;
P_sm = 0* P;
P_sm (:,:,T) = P(T);
x_sm(T) = x(T);
for i = fliplr(2:T-1)
   L = P(:,:,i) * A' /(P(:,:,i+1)) ;
   x_sm (:,i)= x(:,i) + L*(x_sm(:,i+1)-x(:,i+1));
   P_sm(:,:,i) = P(:,:,i) + L*(P_sm(:,:,i+1)-P(:,:,i+1))*L';

end
x_sm(:,1)=x(:,1);





end

