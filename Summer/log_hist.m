function [ log_ll] = log_hist( alpha,bid_side,support_num,tick_size )
    hist_count= zeros(support_num,1);
    for j = 1 : length(bid_side.abs_diff)
       ind = floor(bid_side.abs_diff(j)/tick_size)+1;
       if ind <support_num
       hist_count(ind,1) = hist_count(ind,1) + round(bid_side.Q(j));
       end
    end 
    log_ll = sum(hist_count.* log(alpha));

    

end

