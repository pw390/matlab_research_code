%To repeat the same process of Lin's to get 
filename = 'SNP_3.csv';
[bid_side, ask_side, mid_price_l, Time] = Data_parse(filename,5e4,1);

%% parameter settings

N = 1000;
shape_parameter = 1.5;
scale_parameter = 5e-5;
start_time = 700;
end_time = 800;
tick_size = 1e-5;
T_duration = end_time - start_time+1;
support_num = 60; % support number of multinomial distribution

bid_param_map = zeros(3,T_duration);
ask_param_map = zeros(3,T_duration);
ask_var_map = zeros(4,T_duration);
bid_var_map = zeros(4,T_duration);
bid_weight_map = zeros(N,T_duration);
ask_weight_map = zeros(N,T_duration);

sigma_shape = 0.1;
sigma_scale = 3e-5;
sigma_shift = 1e-5;
Truncate = 0;
if Truncate>0
    for i = 1:length(ask_side)
        t_a= find(ask_side(i).abs_diff <=Truncate*tick_size);
        t_b= find(bid_side(i).abs_diff <=Truncate*tick_size);        
        ask_side(i).abs_diff = ask_side(i).abs_diff(t_a);
        ask_side(i).Q = ask_side(i).Q(t_a);
        bid_side(i).abs_diff = bid_side(i).abs_diff(t_b);
        bid_side(i).Q = bid_side(i).Q(t_b);
    end
end

%% bid Initilasiation
clear weight
alpha =shape_parameter*ones(N,1);
beta =scale_parameter*ones(N,1);
shift = zeros(N,1);
j = 1;
for t = start_time: end_time
    disp(t)
    [alpha, beta,shift, pdist] = sample_discrete_gamma(support_num, sigma_shape, sigma_scale,sigma_shift,...
   alpha,beta,shift,tick_size,bid_side(t).abs_diff(1));
    disp('Finish sampling')
    for i = 1 : N
        weight(i,1) = log_gamma_hist( pdist(:,i), shift(i),bid_side(t),support_num,tick_size);
    end
    bid_weight_map (:,j)= weight;
    disp('Finish weight calculation')
    %weight normalisation
    weight  = weight_normalisation(weight);
    % stratified sampling
    [alpha,beta,shift,weight ] = stratified_sampling_multi( alpha,beta,shift,weight);
    disp('Finish resampling')
    bid_param_map(1,j) = mean(alpha);
    bid_param_map(2,j) = mean(beta);
    bid_param_map(3,j) = mean(shift);
    bid_var_map(1,j) = var(alpha);
    bid_var_map(2,j) = var(beta);
    bid_var_map(3,j) = var(shift);
    j=j+1;
end
%% ask Initilasiation
clear weight
alpha =shape_parameter*ones(N,1);
beta =scale_parameter*ones(N,1);
shift = zeros(N,1);

j = 1;
for t = start_time: end_time
    disp(t)
    [alpha, beta,shift, pdist] = sample_discrete_gamma(support_num, sigma_shape, sigma_scale,sigma_shift,...
   alpha,beta,shift,tick_size,ask_side(t).abs_diff(1));
    for i = 1 : N
        weight(i,1) = log_gamma_hist( pdist(:,i), shift(i),ask_side(t),support_num,tick_size);
    end
    ask_weight_map (:,j)= weight;
    %weight normalisation
    weight  = weight_normalisation(weight);
    % stratified sampling
    [alpha,beta,shift,weight ] = stratified_sampling_multi( alpha,beta,shift,weight);
    ask_param_map(1,j) = mean(alpha);
    ask_param_map(2,j) = mean(beta);
    ask_param_map(3,j) = mean(shift);
    ask_var_map(1,j) = var(alpha);
    ask_var_map(2,j) = var(beta);
    ask_var_map(3,j) = var(shift);
    j=j+1;
end

%% log likelihood
weight_map = bid_weight_map;
marginal_ll=0;
for i = 1: T_duration
    max_weight = max(weight_map(:,i));
    marginal_ll= marginal_ll+max(weight_map(:,i));
    n_weight = exp(weight_map(:,i) - max_weight);
    marginal_ll = marginal_ll+log(sum(n_weight));
end
marginal_ll = marginal_ll - T_duration*log(N);
disp(marginal_ll)
%% heat map plot

figure
i =1;
for t = start_time : end_time
    disp(t)
    x = (1:support_num)*tick_size;
    cdf1 = gamcdf(x, bid_param_map(1,i),bid_param_map(2,i)); 
    balpha = (cdf1-[0,cdf1(1:end-1)])';
    balpha = balpha/sum(balpha);
    temp_max= max(balpha);
    
    cdf1 = gamcdf(x, ask_param_map(1,i),ask_param_map(2,i)); 
    salpha = (cdf1-[0,cdf1(1:end-1)])';
    salpha = salpha/sum(salpha);
    temp_max= max(max(salpha),temp_max);
    for d = 1 : support_num
        %%Construct heat map with position defined by price and time, colour
        %%defined by quantity
    rectangle('Position',[Time(t),mid_price_l(t)-d*tick_size+bid_param_map(3,i),Time(t+1)-Time(t),tick_size],'Facecolor'...
       ,hsv2rgb([2/3-2/3*balpha(d)/temp_max,1.0,1.0]),'edgecolor','none'); 
    end

    for d = 1 : support_num
        %%Construct heat map with position defined by price and time, colour
        %%defined by quantity
    rectangle('Position',[Time(t),mid_price_l(t)+(d-1)*tick_size-ask_param_map(3,i),Time(t+1)-Time(t),tick_size],'Facecolor'...
       ,hsv2rgb([2/3-2/3*salpha(d)/temp_max,1.0,1.0]),'edgecolor','none'); 
    end
    i= i+1;
end
xlim([Time(start_time),Time(end_time)]);
hold on 
plot(Time(start_time:end_time),mid_price_l(start_time:end_time),'-','color','k','LineWidth',2);legend('mid price');
ylabel('Price [USD/EUR]');xlabel('Time');
title('Heat map of orderbook rebuilt based on Gamma distribution (reference price: mid-price)');
colorbar();caxis([0 1]);
%set(gca,'color',hsv2rgb([2/3 1 1]));


%% plot with quantity correct answer
figure
i =1;
for t = start_time : end_time
    disp(t)
    x = (1:support_num)*tick_size;
    cdf1 = gamcdf(x, bid_param_map(1,i),bid_param_map(2,i)); 
    balpha = (cdf1-[0,cdf1(1:end-1)])';
    sequence = find(bid_side(t).abs_diff<=(bid_param_map(3,i)+support_num*tick_size)...
        & bid_side(t).abs_diff>bid_param_map(3,i) );
    balpha = balpha/sum(balpha)*sum(bid_side(t).Q(sequence));
    temp_max= max(balpha);
    
    cdf1 = gamcdf(x, ask_param_map(1,i),ask_param_map(2,i)); 
    salpha = (cdf1-[0,cdf1(1:end-1)])';
    sequence = find(ask_side(t).abs_diff<=(ask_param_map(3,i)+support_num*tick_size)...
        & ask_side(t).abs_diff>ask_param_map(3,i) );
    salpha = salpha/sum(salpha)*sum(ask_side(t).Q(sequence));
    temp_max= max(max(salpha),temp_max);
    for d = 1 : support_num
        %%Construct heat map with position defined by price and time, colour
        %%defined by quantity
    rectangle('Position',[t,mid_price_l(t)-d*tick_size+bid_param_map(3,i),1,tick_size],'Facecolor'...
       ,hsv2rgb([2/3-2/3*balpha(d)/temp_max,1.0,1.0]),'edgecolor','none'); 
    end

    for d = 1 : support_num
        %%Construct heat map with position defined by price and time, colour
        %%defined by quantity
    rectangle('Position',[t,mid_price_l(t)+(d-1)*tick_size-ask_param_map(3,i),1,tick_size],'Facecolor'...
       ,hsv2rgb([2/3-2/3*salpha(d)/temp_max,1.0,1.0]),'edgecolor','none'); 
    end
    i= i+1;
end
xlim([start_time,end_time]);
hold on 
plot(start_time:end_time,mid_price_l(start_time:end_time),'-','color','k','LineWidth',2);
lgd=legend('mid-price m_t');
lgd.FontSize= 15;
ylabel('Price [USD/EUR]','FontSize',18);xlabel('Time','FontSize',18);
%title('Heat map of orderbook rebuilt based on Gamma distribution with Quantity (reference price: mid-price)');
colorbar();caxis([0 1]);
hold on
%y1=get(gca,'ylim');
%plot([Time(738),Time(738)],y1)
%plot([Time(746),Time(746)],y1)
%plot([Time(752),Time(752)],y1)
%set(gca,'color',hsv2rgb([2/3 1 1]));
%% plot parameter
figure;
subplot(3,1,1);
filter_range =start_time:end_time;
param_map = bid_param_map;
var_map = bid_var_map;
alpha_est = param_map(1,:);
alpha_std = sqrt(var_map(1,:));
beta_est = param_map(2,:);
beta_std = sqrt(var_map(2,:));
gammashift_est = param_map(3,:);
gammashift_std = sqrt(var_map(3,:));
plot(filter_range,alpha_est,'red');
errorbar(filter_range,alpha_est,alpha_std,'red');
xlabel('Time point');xlim([start_time end_time]);
ylabel('Alpha');

subplot(3,1,2);
plot(filter_range,beta_est,'red');
errorbar(filter_range,beta_est,beta_std,'red');
xlabel('Time point');xlim([start_time end_time]);
ylabel('Beta');

subplot(3,1,3);
plot(filter_range,gammashift_est,'red');
errorbar(filter_range,gammashift_est,gammashift_std,'red');
xlabel('Time point');xlim([start_time end_time]);
ylabel('Gammashift');


%%  curve fitting
figure
plot_time = 746;
Truncate = 60;
if Truncate>0
        t_a= find(ask_side(plot_time).abs_diff <=Truncate*tick_size);
        t_b= find(bid_side(plot_time).abs_diff <=Truncate*tick_size);        
        ask_side(plot_time).abs_diff = ask_side(plot_time).abs_diff(t_a);
        ask_side(plot_time).Q = ask_side(plot_time).Q(t_a);
        bid_side(plot_time).abs_diff = bid_side(plot_time).abs_diff(t_b);
        bid_side(plot_time).Q = bid_side(plot_time).Q(t_b);
end
area_bid =  sum(bid_side(plot_time).Q);
area_ask = sum(ask_side(plot_time).Q);
location = [mid_price_l(plot_time)-bid_side(plot_time).abs_diff];
quantity = round([bid_side(plot_time).Q]);
ylabel('Normalised Order Qty');
bar(location,quantity,'Facecolor','c','edgecolor','none');
quantity = round( ask_side(plot_time).Q);
hold on 
location =[mid_price_l(plot_time)+ask_side(plot_time).abs_diff];
bar(location,quantity,'Facecolor','r','edgecolor','none'); 
pt= plot_time - start_time + 1;
x= (1:support_num)*tick_size;
b_axis = -bid_param_map(3,pt) + x-0.5*tick_size;
a_axis = -ask_param_map(3,pt) + x-0.5*tick_size;
cdf1 = gamcdf(x, bid_param_map(1,pt),bid_param_map(2,pt)); 
balpha = (cdf1-[0,cdf1(1:end-1)])';
bid_p = balpha/sum(balpha)*area_bid;
cdf1 = gamcdf(x, ask_param_map(1,pt),ask_param_map(2,pt)); 
balpha = (cdf1-[0,cdf1(1:end-1)])';
ask_p = balpha/sum(balpha)*area_ask;
bar(mid_price_l(plot_time)-b_axis,bid_p,'Facecolor','none','edgecolor','k');
hold on;
bar(mid_price_l(plot_time)+(a_axis),(ask_p),'Facecolor','none','edgecolor','g');
xlabel('Price [USD/EUR]','FontSize',15);
ylabel('Volume','FontSize',15);
ldg=legend('bid orders','ask orders','Multi-Ga on the bid side', 'Multi-Ga on the ask side')
ldg.FontSize = 15
y1=get(gca,'ylim');
hold on
%plot([mid_price_l(plot_time),mid_price_l(plot_time)],y1,'r');
%plot([mid_price_l(plot_time)+bid_param_map(3,pt),mid_price_l(plot_time)+bid_param_map(3,pt)],y1,'b');
%plot([mid_price_l(plot_time)-ask_param_map(3,pt),mid_price_l(plot_time)-ask_param_map(3,pt)],y1,'g');

%title(strcat('Curve fitting at time point ',num2str(plot_time)))
%%
weight_map = ask_weight_map;
marginal_ll=0;
s=21;
for i = s: T_duration
    max_weight = max(weight_map(:,i));
    marginal_ll= marginal_ll+max_weight;
    n_weight = exp(weight_map(:,i) - max_weight);
    marginal_ll = marginal_ll+log(sum(n_weight));
end
marginal_ll = marginal_ll - (T_duration+1-s)*log(N);
disp(marginal_ll)


