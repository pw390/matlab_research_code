function [ prob_dist] = bin_pro( a,b,c,mu,support_num,below_base,tick_size )
N = length(a);
prob_dist = zeros(support_num,N);
parfor i = 1:N
    
    %pd1 = makedist('Stable','alpha',a(i),'beta',b(i),'gam',c,'delta',mu(i));
    pars=[a(i),b(i),c,mu(i)];
    x = (-below_base+1:1:support_num-below_base)*tick_size;
    pdf_1 = stable_pdf(x, pars);
    %cdf1 = cdf(pd1,x); 
    %x = (0:support_num-1)*tick_size;
    %cdf2 = cdf(pd1,x); 
    prob_dist(:,i) = pdf_1';
    prob_dist(:,i)= prob_dist(:,i)/sum(prob_dist(:,i));
end


end

