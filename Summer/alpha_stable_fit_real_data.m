 %% ReadIn Data
filename = 'SNP_2.csv';
[bid_side, ask_side, mid_price_l] = Data_parse_primitive(filename,1e5);
%%
example_id = 25;
tick_size = 1e-5;
depth = 40;
bin = depth;
%% form random sample 
figure
price_distance = bid_side(example_id).price - mid_price_l(example_id);
Quantity = round(bid_side(example_id).Q);

if depth
    t = find(abs(price_distance)<depth*tick_size);
    price_distance = price_distance(t);
    Quantity = Quantity(t);
end
N = sum(Quantity);
x= repelem(price_distance,Quantity);

[counts,centers]=hist(x,bin);
w = centers(2)-centers(1);
bar(centers,counts)
y = centers(1): w : centers(bin);
hold on
par = stable_fit_init(x);
par = stable_fit_mle2d(x,par);
pdf = stable_pdf(y,par);
plot(y,pdf*N*w,'r');
hold on
[phat,pci] = gamfit(abs(x));
gpdf2 = gampdf(y,phat(1),phat(2));
plot(gpdf2*N*w,'y')
%%
figure
price_distance = ask_side(example_id).price - mid_price_l(example_id);
Quantity = round(ask_side(example_id).Q);
if depth

    t = find(abs(price_distance)<depth*tick_size);
    price_distance = price_distance(t);
    Quantity = Quantity(t);
end

N = sum(Quantity);
x= repelem(price_distance,Quantity); 
[counts,centers]=hist(x,bin);
w = centers(2)-centers(1);
bar(centers,counts)
y = centers(1): w : centers(bin);gir 
hold on
par = stable_fit_init(x);
par = stable_fit_mle2d(x,par);
pdf = stable_pdf(y,par);
plot(y,pdf*N*w,'r');

hold on
[phat,pci] = gamfit(abs(x));
gpdf2 = gampdf(y,phat(1),phat(2));
plot(y,gpdf2*N*w,'y')


