function pars = stable_fit_init(data, parametrization,usedll)
% pars = stable_fit_initC(data, parametrization)
% McCulloch [1] alpha-stable parameters estimation.
%    data: vector of random data
%    pars: parameters of the alpha-estable distributions.
%                 pars=[alpha,beta,sigma,mu];
%
% Copyright (C) 2013. Javier Royuela del Val
%                     Federico Simmross Wattenberg
%
% [1] John H. McCulloch. Simple consistent estimators of stable distribution pa-
%     rameters. Communications in Statistics – Simulation and Computation,
%     15(4):1109–1136, 1986.

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; version 3 of the License.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; If not, see <http://www.gnu.org/licenses/>.
%
%
%  Javier Royuela del Val.
%  E.T.S.I. Telecomunicación
%  Universidad de Valladolid
%  Paseo de Belén 15, 47002 Valladolid, Spain.
%  jroyval@lpi.tel.uva.es    
%

if nargin < 3
    usedll = false;
end
if nargin < 2 || isempty(parametrization)
    parametrization = 0;
end

if isempty(data)
    error('data is empty');
end
if (parametrization<0 || parametrization >1)
    error('parametrization must be 0 or 1');
end


n=length(data);

if (n<2)
    error('at least two data points are needed');
end

if usedll
    pars = zeros(1,4);
    pars = calllib('libstable','stable_fit_init_matlab',pars,data,n,parametrization);
else
    pars = stable_matlab_mex('stable_fit_init', data, parametrization);
end

end