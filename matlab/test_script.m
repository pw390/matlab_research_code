%% Functions evaluation
x = -5:0.01:20;
pars = [1.25 1 1 0];
parametrization = 0;
p = 0.001:0.001:0.999;
tic; pdf = stable_pdf(x, pars, parametrization,[],[],false); toc;
tic; cdf = stable_cdf(x, pars, parametrization,[],[],false); toc;
tic; inv = stable_inv(p, pars, parametrization,[],[],false); toc;

figure(1);
subplot(2,3,1);
plot(x,pdf); xlabel('x'); ylabel('pdf');
subplot(2,3,2);
plot(x,cdf); xlabel('x'); ylabel('cdf');
subplot(2,3,3);
plot(p,inv); xlabel('p'); ylabel('cdf^{-1}');


%% Random samples generation and parameter estimation
tic; rnd = stable_rnd(1000, [1.25 1 1 0],0,0,false); toc;

tic;  pars_init                 = stable_fit_init(rnd, 0, false); toc;
tic; [pars_k, status_k]         = stable_fit_koutrouvelis(rnd, pars_init, parametrization,[],[],false); toc;
tic; [pars_mle2d, status_mle2d] = stable_fit_mle2d(rnd, pars_k, parametrization,[],[],false); toc;
tic; [pars_mle, status_mle]     = stable_fit_mle(rnd, pars_mle2d, parametrization,[],[],false); toc;

pdf_init  = stable_pdf(x, pars_init, parametrization);
pdf_k     = stable_pdf(x, pars_k, parametrization);
pdf_mle2d = stable_pdf(x, pars_mle2d, parametrization);
pdf_mle   = stable_pdf(x, pars_mle, parametrization);

cdf_init  = stable_cdf(x, pars_init, parametrization);
cdf_k     = stable_cdf(x, pars_k, parametrization);
cdf_mle2d = stable_cdf(x, pars_mle2d, parametrization);
cdf_mle   = stable_cdf(x, pars_mle, parametrization);

subplot(2,3,4); cla
hist(rnd,-5:0.5:50); xlabel('x'); ylabel('count');
hold all;
plot(x,pdf*length(rnd)*0.5);
plot(x,pdf_init*length(rnd)*0.5);
plot(x,pdf_k*length(rnd)*0.5);
plot(x,pdf_mle2d*length(rnd)*0.5);
plot(x,pdf_mle*length(rnd)*0.5);
hold off
xlim([-5 50]);
legend('Hist','True pdf','McCulloch','Koutrouvelis','ML2D','ML');

subplot(2,3,5); cla
ecdf(rnd);
xlabel('x'); ylabel('ECDF');
hold all;
plot(x,cdf);
plot(x,cdf_init);
plot(x,cdf_k);
plot(x,cdf_mle2d);
plot(x,cdf_mle);
hold off
xlim([-5 50]);

legend('ECDF', 'True pdf','McCulloch','Koutrouvelis','ML2D','ML');

