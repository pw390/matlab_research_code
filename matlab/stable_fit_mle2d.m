function [pars, status] = stable_fit_mle2d(data, pars_init, parametrization, relTol, threads,usedll)
% [pars, status] = stable_fit_mle2dC(data, pars_init, parametrization)
% Modified 2D Maximum likelihood estimation of alpha-stable parameters.
%  Inputs:
%    data:  vector of random data
%    pars_init:    initial guess of parameters estimation. If none is provided,
%           McCulloch estimation is done before ML.
%    parametrization:  parameterization employed (view [1] for details)
%             0:   mu=mu_0
%             1:   mu=mu_1
%  Outputs:
%    pars = [alpha,beta,sigma,mu_0]. Estimated parameters.
%    status : status>0 indicates some error on the iterative procedure.
%
% Copyright (C) 2013. Javier Royuela del Val
%                     Federico Simmross Wattenberg

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; version 3 of the License.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; If not, see <http://www.gnu.org/licenses/>.
%
%
%  Javier Royuela del Val.
%  E.T.S.I. Telecomunicación
%  Universidad de Valladolid
%  Paseo de Belén 15, 47002 Valladolid, Spain.
%  jroyval@lpi.tel.uva.es    
%
if nargin < 6
    usedll = false;
end
if nargin < 5 || isempty(threads)
    threads = 0;
end
if nargin < 4 || isempty(relTol)
    relTol = 1e-8;
end
if nargin < 3 || isempty(parametrization)
    parametrization = 0;
end
if nargin < 2
    pars_init = [];
end

if (parametrization<0 || parametrization >1)
    error('parametrization must be 0 or 1');
end
if ~isempty(pars_init) && (~isvector(pars_init) || numel(pars_init)~=4)
    error('provided initial parameters must be a four elements vector');
end

n=length(data);
if (n<2)
    error('at least two data points are needed');
end

if usedll
    pars = zeros(1,4);
    [status,pars] = calllib('libstable','stable_fit_mle2d_matlab', ...
            pars, data, n, pars_init, parametrization,relTol,threads); 
else
    [status, pars] = stable_matlab_mex('stable_fit_mle2d', data, pars_init, parametrization, relTol, threads);
end

