#include <stdlib.h>
#include <string.h>

#include "stable_matlab.h"
#include "mex.h"

// #define DEBUG

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
	int cmd_len = 0;
	char * cmd = NULL;
	double * x = NULL;
	double * pars = NULL;
	int parametrization = 0;
	double relTol = 0.0;
	int threads = 0;
	double * xdf = NULL;
	int n = 0;
	double * pars_init = NULL;
	double * status = NULL;
	double * data = NULL;
	
	int seed;
	
	#ifdef DEBUG
	mexPrintf("\nInitMexFunction\n");
	return;
	#endif
	
    if (nrhs < 1)
        mexErrMsgIdAndTxt("libstable:main", "At least one argument is needed");

    if (!mxIsChar(prhs[0]))
        mexErrMsgIdAndTxt("libstable:main", "First argument must be a string.");

    
    cmd_len = mxGetN(prhs[0]);
    cmd = (char*) malloc((cmd_len+1) * sizeof(char));
    if (mxGetString(prhs[0], cmd, cmd_len+1)) {
        mexPrintf("%s, %d\n",cmd, cmd_len);
        free(cmd);
        mexErrMsgIdAndTxt("libstable:main", "Could not read command.");
    }

    
    if (strcmp(cmd, "stable_pdf") == 0 ) {
		#ifdef DEBUG
		mexPrintf("\nstable_pdf\n");
		return;
		#endif
		
		
        if (nlhs != 1)
            mexErrMsgIdAndTxt("libstable:pdf", "Just one output argument is needed");

        x    = mxGetPr(prhs[1]);
        pars = mxGetPr(prhs[2]);
        parametrization = (int) mxGetScalar(prhs[3]);
        relTol = mxGetScalar(prhs[4]);
        threads   = (int) mxGetScalar(prhs[5]);

        n = mxGetNumberOfElements(prhs[1]);

        plhs[0] = mxCreateDoubleMatrix((mwSize) 1, (mwSize) n, false);
        xdf = mxGetPr(plhs[0]);

        stable_pdf_matlab(xdf,x,n,pars,parametrization,relTol,threads);
    }

    if (strcmp(cmd, "stable_cdf") == 0 ) {
        if (nlhs != 1)
            mexErrMsgIdAndTxt("libstable:cdf", "One one output argument is needed");

        x    = mxGetPr(prhs[1]);
        pars = mxGetPr(prhs[2]);
        parametrization = (int) mxGetScalar(prhs[3]);
        relTol = mxGetScalar(prhs[4]);
        threads   = (int) mxGetScalar(prhs[5]);

        n = mxGetNumberOfElements(prhs[1]);

        plhs[0] = mxCreateDoubleMatrix(1, n, false);
        xdf = mxGetPr(plhs[0]);

        stable_cdf_matlab(xdf,x,n,pars,parametrization,relTol,threads);
    }

    if (strcmp(cmd, "stable_inv") == 0 ) {
        if (nlhs != 1)
            mexErrMsgIdAndTxt("libstable:inv", "Just one output argument is needed");

        x    = mxGetPr(prhs[1]);
        pars = mxGetPr(prhs[2]);
        parametrization = (int) mxGetScalar(prhs[3]);
        relTol = mxGetScalar(prhs[4]);
        threads   = (int) mxGetScalar(prhs[5]);

        n = mxGetNumberOfElements(prhs[1]);

        plhs[0] = mxCreateDoubleMatrix(1, n, false);
        xdf = mxGetPr(plhs[0]);

        stable_q_matlab(xdf,x,n,pars,parametrization,relTol,threads);
    }

    if (strcmp(cmd, "stable_rnd") == 0 ) {
        if (nlhs != 1)
            mexErrMsgIdAndTxt("libstable:rnd", "Just one output argument is needed");

        n = (int) mxGetScalar(prhs[1]);
        pars = mxGetPr(prhs[2]);
        parametrization = (int) mxGetScalar(prhs[3]);
        seed = (int) mxGetScalar(prhs[4]);

        plhs[0] = mxCreateDoubleMatrix(1, n, false);
        xdf = mxGetPr(plhs[0]);

        stable_rnd_matlab(xdf,n,pars,parametrization,seed);
    }

    if (strcmp(cmd, "stable_fit_init") == 0 ) {
        if (nlhs != 1)
            mexErrMsgIdAndTxt("libstable:fit_init", "Just one output argument is needed");

        data = mxGetPr(prhs[1]);
        parametrization = (int) mxGetScalar(prhs[2]);
        
        n = mxGetNumberOfElements(prhs[1]);
        
        plhs[0] = mxCreateDoubleMatrix(1, 4, false);
        pars = mxGetPr(plhs[0]);

        stable_fit_init_matlab(pars,data,n,parametrization);
    }

    if (strcmp(cmd, "stable_fit_koutrouvelis") == 0 ) {
        if (nlhs != 2)
            mexErrMsgIdAndTxt("libstable:fit_k", "Two output arguments are needed");

        data      = mxGetPr(prhs[1]);

        pars_init = NULL;
        if (!mxIsEmpty(prhs[2]))
            pars_init = mxGetPr(prhs[2]);

        parametrization = (int) mxGetScalar(prhs[3]);
        relTol = mxGetScalar(prhs[4]);
        threads   = (int) mxGetScalar(prhs[5]);

        n = mxGetNumberOfElements(prhs[1]); 

        plhs[0] = mxCreateDoubleScalar(5.0);
        status = mxGetPr(plhs[0]);

        plhs[1] = mxCreateDoubleMatrix(1, 4, false);
        pars = mxGetPr(plhs[1]);

        *status = (double) stable_fit_koutrouvelis_matlab(pars,data,n,pars_init,parametrization,relTol,threads);
    }

    if (strcmp(cmd, "stable_fit_mle2d") == 0 ) {
        if (nlhs != 2)
            mexErrMsgIdAndTxt("libstable:fit_mle2d", "Two output arguments are needed");

        data      = mxGetPr(prhs[1]);

        pars_init = NULL;
        if (!mxIsEmpty(prhs[2]))
            pars_init = mxGetPr(prhs[2]);

        parametrization = (int) mxGetScalar(prhs[3]);
        relTol = mxGetScalar(prhs[4]);
        threads   = (int) mxGetScalar(prhs[5]);

        n = mxGetNumberOfElements(prhs[1]);
        
        plhs[0] = mxCreateDoubleScalar(5.0);
        status = mxGetPr(plhs[0]);

        plhs[1] = mxCreateDoubleMatrix(1, 4, false);
        pars = mxGetPr(plhs[1]);

        *status = (double) stable_fit_mle2d_matlab(pars,data,n,pars_init,parametrization,relTol,threads);
    }

    if (strcmp(cmd, "stable_fit_mle") == 0 ) {
        if (nlhs != 2)
            mexErrMsgIdAndTxt("libstable:fit_mle", "Two output arguments are needed");

        data      = mxGetPr(prhs[1]);

        pars_init = NULL;
        if (!mxIsEmpty(prhs[2]))
            pars_init = mxGetPr(prhs[2]);

        parametrization = (int) mxGetScalar(prhs[3]);
        relTol = mxGetScalar(prhs[4]);
        threads   = (int) mxGetScalar(prhs[5]);

        n = mxGetNumberOfElements(prhs[1]);
        
        plhs[0] = mxCreateDoubleScalar(5.0);
        status = mxGetPr(plhs[0]);

        plhs[1] = mxCreateDoubleMatrix(1, 4, false);
        pars = mxGetPr(plhs[1]);

        *status = (double) stable_fit_mle_matlab(pars,data,n,pars_init,parametrization,relTol,threads);
    }

    free(cmd);
}

